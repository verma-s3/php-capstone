<?php if(!empty($_SESSION['message'])) : ?>
<div class="message">
    <p><?=esc($_SESSION['message'])?></p>
</div>
<?php 
unset($_SESSION['message']); 
endif; 
?>