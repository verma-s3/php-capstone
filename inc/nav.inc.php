<!-- navigation inside the nav  -->
<nav id="main">
  <a id="toggle_menu" href="#navlist"><img src="Images/menu-white.png" alt="menu" /></a>
  <ul id="navlist">
    <li <?=($title=='Home') ? 'class="current"' : "" ;?>>
      <a href="index.php" title="Home Page">Home</a>
    </li>
    <li <?=($title=='About') ? 'class="current"' : "" ;?>>
      <a href="about.php" title="About us Page">About us</a>
    </li>
    <li <?=($title=='Photo') ? 'class="current"' : "" ;?>>
      <a href="photo.php" title="Photo Page">Photo</a>
    </li>
    <li <?=($title=='Services') ? 'class="current"' : "" ;?>>
      <a href="services.php" title="Services Page">Services</a>
    </li>
    <li <?=($title=='Contact') ? 'class="current"' : "" ;?>>
      <a href="contact.php" title="Contact us Page">Contact us</a>
    </li>
  </ul>
</nav><!-- nav ending -->