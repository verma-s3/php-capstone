<header><!-- header starting -->
  <div id="inner_header"><!-- inner_div starting -->
    <img src="Images/logo.png" alt="logo" class="logo"  />
    <div class="phone"><!-- div for phone starting -->
      <img src="Images/p.png" alt="phone_icon" style="height: 20px; width: 20px;" />
      <span>+91 97812-40006</span>
    </div><!-- div for phone ending -->
    <?php if(empty($_SESSION['log_in']) || $_SESSION['log_in'] == false) : ?>
      <input type="button" value="REGISTER" onclick="window.location.href='registration.php'" class="register" />
      <input type="button" value="LOGIN" onclick="window.location.href='login.php'" class="login"/>
    <?php else: ?>
      <input type="button" value="PROFILE" onclick="window.location.href='profile.php'" class="register" />
      <input type="button" value="LOGOUT" onclick="window.location.href='login.php?logout=1'" class="login"/>
    <?php endif; ?>    

    <div id="social_media"><!-- div for button starting -->
      <a href="https://www.facebook.com/" title="facebook link"><img src="Images/facebook.png" alt="social media icons" style="margin-right: 25px;" class="facebook"/></a>
      <a href="https://www.instagram.com/" title="instagram link"><img src="Images/instagram.png" alt="social media icons" style="margin-right: 25px;" class="instagram" /></a>
      <a href="https://twitter.com/" title="twitter link"><img src="Images/twitter.png" alt="social media icons" class="twitter" /></a>
    </div> <!-- div for button ending -->
    
    <!-- PHP Navigation File -->
    <?php require __DIR__.'/../inc/nav.inc.php'; ?>

  </div><!-- inner_div ending -->
</header><!-- header ending -->