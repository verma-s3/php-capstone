<!DOCTYPE HTML>

<html lang="en">
  <head>
    <title><?=site_name.esc($title); ?>
    </title> <!-- comapany name in title for good accessibility -->
    <meta charset="utf-8" />
    <!-- For Responsive Design -->
    <meta name="viewport" content="width=device-width" />
    <link rel="icon" href="Images/favicon.jpg" />
    <link rel="apple-touch-icon" sizes="57x57" href="Images/apple-icon-57x57.jpg" />
    <link rel="apple-touch-icon" sizes="72x72" href="Images/apple-icon-72x72.jpg" />
    <link rel="apple-touch-icon" sizes="114x114" href="Images/apple-icon-114x114.jpg" />
    <link rel="apple-touch-icon" sizes="144x144" href="Images/apple-icon-144x144.jpg" /> 
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700%7cCookie:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700%7cRoboto+Slab:400,700" rel="stylesheet">
    <!-- external stylesheet -->
    <link rel="stylesheet" type="text/css" href="styles/style.css" media="screen and (min-width: 767px)" />
    <link rel="stylesheet" type="text/css" href="styles/print.css" media="print" />
    <link rel="stylesheet" type="text/css" href="styles/media.css" media="all and (max-width:960px)" />
    <link rel="stylesheet" type="text/css" href="styles/mobile-media.css" media="all and (max-width:767px)" />
    
    
    <!--[if LTE IE 9]>
      <link rel="stylesheet" type="text/css" href="styles/ie.css" />
      <script>
        document.createElement('header');
        document.createElement('nav');
        document.createElement('footer');
        document.createElement('article');
        document.createElement('section');
        document.createElement('main');
        document.createElement('aside');
      </script>
    <![endif]-->
    <style>
      .error {
        color: #ff0000;
        font-weight: 500;
        margin-left: 10px;
      }

      .message{
        color: #fff;
        background: #000;
        border: 3px solid #fff;
        margin: 40px;
        padding-left: 20px;
      }

    </style>

    <?php
        if($title == 'Home'){
          require __DIR__.'/../css/index.css';
        }
    
        if($title == 'About'){
          require __DIR__.'/../css/about.css';
        }
    
        if($title == 'Photo'){
          require __DIR__.'/../css/photo.css';
        }
    
        if($title == 'Services'){
          require __DIR__.'/../css/service.css';
        }
    
        if($title == 'Contact'){
          require __DIR__.'/../css/contact.css';
        }

        if($title == 'Profile'){
          require __DIR__.'/../css/profile.css';
        }

        if($title == 'Registration Form'){
          require __DIR__.'/../css/register.css';
        }

        if($title == 'Login'){
          require __DIR__.'/../css/login.css';
        }

        if($title == 'Service detail'){
          require __DIR__.'/../css/service_deatil.css';
        }

        if($title == 'Add to cart'){
          require __DIR__ .'/../css/addtocart.css';
        }

        if($title == 'Checkout'){
          require __DIR__.'/../css/checkout.css';
        }
    ?>
  
  </head>