<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitb3bf1ed30d01d9d1fd0fb2b5a227d901
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/classes',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitb3bf1ed30d01d9d1fd0fb2b5a227d901::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitb3bf1ed30d01d9d1fd0fb2b5a227d901::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
