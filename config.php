<?php

// start of output buffering. 
ob_start();

// starting the session
session_start();

require 'functions.php';
require __DIR__ .'/vendor/autoload.php';

/**
   * Configration File
   * last_update: 2019-08-02
   * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
   * Site name : Khera Digital Studio and Color Lab
   */
// init_set --> Sets the value of a configuration option
ini_set("display_errors",1);
ini_set('error_reporting',E_ALL);

// set sitename Credentials
define("site_name","Khera Color Lab and Digital Studio ");

// set Database Credentials

// define('DB_DSN', 'mysql:host=localhost;dbname=capstone');
// define('DB_USER', 'sverma_user');
// define('DB_PASS', 'Muw09$Ssa89');

define('DB_DSN', 'mysql:host=localhost;dbname=photostudio');
define('DB_USER', 'root');
define('DB_PASS', '');

// 1. Create DB connection
$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
// set the $dbh to display errors if there are any
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// CSRF token 
// Reference: STEVE's code
// generate a csrf token if we need one
if(empty($_SESSION['csrf'])) {
    $_SESSION['csrf'] = md5( uniqid() . time() );
}

// test every POST submission for the csrf token
if('POST' == $_SERVER['REQUEST_METHOD']) {
    if(empty($_POST['csrf']) || $_POST['csrf'] != $_SESSION['csrf']) {
        die('Your session appears to have expired.  CSRF token mismatch!');
    }
}

// using model class for dependency injection
use App\Model;

Model::init($dbh);

//check for admin panel login
// $uri = $_SERVER['REQUEST_URI'];
// if($uri == '/admin/'){
// 	if(!empty($_SESSION['log_in']) || $_SESSION['log_in'] == true){
// 		var_dump($_SESSION['admin']);
// 		die;
// 	}
// }

/**
 * Resturn all the results from log table in descendign order
 * @return records
 */
function allEvent()
{	
	global $dbh;
	$query = "SELECT * from log ORDER BY event DESC LIMIT 20";
	$stmt = $dbh->prepare($query);
	$stmt->execute();
	return $stmt->fetchAll(\PDO::FETCH_ASSOC);		
}

/**
 * writing the log to databse
 * @return [type] [description]
 */
function logWrite()
{
	// creata an event
	// date/time request
	if( $_SERVER['REQUEST_TIME'] !== ''){
		$time = $_SERVER['REQUEST_TIME'];
		$date = date("Y-m-d H:i:s", $time);
	}
	// type of request
	if($_SERVER['REQUEST_METHOD'] !== ''){
		$request = $_SERVER['REQUEST_METHOD'];
	}
	// whatever was requested eg: /books
	if($_SERVER['REQUEST_URI'] !== ''){
		$uri = $_SERVER['REQUEST_URI'];
		$uri = trim($uri,'/');
	}

	// type of browser
	if($_SERVER['HTTP_USER_AGENT'] !== ''){
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
	}

	$event = $date." | ".$uri." | ".$request." | ".$user_agent;
	// cehck for not empty $event 
	if(!empty($event))
	{	
		global $dbh;
		//insert the event in the database
		$query = 'INSERT into log(event)
					values (:event)';
		$params = array(':event'=>$event);
		$stmt= $dbh->prepare($query);
		$stmt->execute($params);
		$id = $dbh->lastInsertId();
		return $id;
	}//endif 
}//end of logWrite function


  //writing the log into databse;
  logWrite();
  //displaying log to brower
  $asc = allEvent();
?>
