<?php

namespace App;

class Validator
{
	/**
	 * Array for tracking validation errors
	 * @var array
	 */
	protected $errors = [];

	/**
	 * post with protectted access modfiers
	 * @var string
	 */
	protected $post;

	/**
	 * constructor function to trim the spaces from field value.
	 */
	public function __construct()
	{
		//foreach(filter_input_array(INPUT_POST) as $key => $value){
		foreach($_POST as $key => $value){
			$this->post[$key] = trim($value);
		}//endforeach
	}// constructor function end
    
    /**
     * showing error meassage for form field that are mandatory
     * @param  string $field -- value of the form field
     */
    public function required($field)
    {
		if(empty($this->post[$field])){
			$label = $this->label($field);
			$this->setError($field, "$label is required field");
		}//endif
	}// required function end

	/**
	 * first_name, last_name, city, province, country validation
	 * @param string $field -- value of string field
	 */
	public function stringCheck($field)
	{
		// pattern with minimun 3 and maximun 100 characters.
		// {3,100} works as length validator in pattern
		$pattern = '/(^[a-zA-Z\s?\-?]{3,100})+$/';
		if(preg_match($pattern, $this->post[$field]) !== 1)
     	{
     		$label = $this->label($field);
     		$this->setError($field, "Enter a valid $label");
     	}//endif
	}//stringCheck Function end

	/**
	 * street field validation
	 * @param  String $field -- value of street field from form
	 */
	public function streetCheck($field)
	{
		// A-z belongs to A-Z and a-z 
		// this means it qualifies for upper and lower case letter as well.
		$pattern = "/^[\dA-z\s]{3,50}$/";
		if(preg_match($pattern, $this->post[$field]) !== 1)
     	{
     		$label = $this->label($field);
     		$this->setError($field, "Enter a valid $label");
     	}//endif
	}//street check function end

	/**
	 * postal code validation
	 * @param  String $field -- value of postalcode from form
	 */
	public function postalCodeCheck($field)
	{
		$pattern = '/^[A-Za-z]\d[A-Za-z]\s?\d[A-Za-z]\d$/';
		if(preg_match($pattern, $this->post[$field]) !== 1)
     	{
     		$this->setError($field, 'Please enter a valid Postal Code');
     	}//endif

	}// postalcodeCheck Function end

	/**
	 * Phone validation
	 * @param  String $field -- phone value from phone form field
	 */
    public function phoneCheck($field)
    {
    	$pattern = '/^\+?(\d{1,3})?\s?([0-9]{3})-?([0-9]{3})-?([0-9]{4})/';
     	if(preg_match($pattern, $this->post[$field]) !== 1)
     	{
     		$this->setError($field, 'Please enter a valid phone number');
     	}//endif
    }// Phonecheck funciton end

    /**
     * check for email uniaueness
     * @param  string $field  email address from the form field
     * @return none
     */
    public function emailUnique($field){

    	global $dbh;
		$query1 = "SELECT * from customer where email_address = :email_address";
      // preparing database
      $stmt1 = $dbh->prepare($query1);

      //paramters to check form email_addrrss exist in databse
      $params1 = array(':email_address' =>$_POST['email_address']);
      //execute database
      $stmt1->execute($params1);
      
      // fetching queries
      $result_data = $stmt1->fetch();

      //if result_data is empty then inset data otherwise display the error message 
      if(!empty($result_data)){
      	$this->setError($field,"Sorry, This email is already taken.");
      }

    }

    /**
     * email address validation
     * @param  String $field -- email value from form field
     */
    public function emailCheck($field)
	{
		if(!filter_var($_POST[$field],FILTER_VALIDATE_EMAIL)){
     		$this->setError($field, "Please provide validate email address" );
    	}// endif
	}//email check function end

	/**
	 * password and password validation validation 
	 * @param  String $field -- password values from form field
	 */
	public function passwordCheck($field)
	{
		$pattern = "/(?=.*[A-Z]+)(?=.*[a-z]+)(?=.*[0-9]+)(?=.*[\!\@\#\$\%\^\&\*\(\)]+).{6,}/";
		if(preg_match($pattern, $this->post[$field]) !== 1)
     	{
     		$this->setError($field, 'Please enter a valid password');
     	}//endif
     	else{
     		$_POST[$field] = password_hash($this->post[$field], PASSWORD_DEFAULT);
     	}
	}// password check funciton end

	/**
	 * comparison of password and password_comform
	 * @param  String $field1 -- value of password from form field
	 * @param  String $field2 -- value of password_confirm form form field
	 */
	public function passwordsComapre($field1, $field2)
	{
		if(!empty($field1)){
			$pass1 = $this->post[$field1];
			$pass2 = $this->post[$field2];
			if(strcmp($pass1, $pass2)!== 0)
			{
				$this->setError($field1,'Password doesnot match');
			}
	    }
	}// end of password compare function
  
  	/**
  	 * sessionquality funciton
  	 * @param  string $field -- session and photo quality field
  	 * @return [type]        [description]
  	 */
    public function sessionQuality($field)
    {
    $pattern = "/^[0-9]{1,}\s?[A-Za-z]{1,}$/";
		if(preg_match($pattern, $this->post[$field]) !== 1)
     	{
     		$label = $this->label($field);
     		$this->setError($field, "Enter a valid $label");
     	}//endif
    }
  	/**
  	 * size funciton
  	 * @param  string $field -- size field
  	 * @return [type]        [description]
  	 */
    public function size($field)
    {
    $pattern = "/^[0-9]{1,3}[\*][0-9]{1,3}$/";
		if(preg_match($pattern, $this->post[$field]) !== 1)
     	{
     		$label = $this->label($field);
     		$this->setError($field, "Enter a valid $label");
     	}//endif
    }
  	/**
  	 * Quality funciton
  	 * @param  string $field --quantity field
  	 * @return [type]        [description]
  	 */
    public function quantity($field)
    {
    $pattern = "/^[0-9]{1,4}$/";
		if(preg_match($pattern, $this->post[$field]) !== 1)
     	{
     		$label = $this->label($field);
     		$this->setError($field, "Enter a valid $label");
     	}//endif
    }
  	/**
  	 * price funciton
  	 * @param  string $field price field
  	 * @return [type]        [description]
  	 */
    public function price($field)
    {
    $pattern = "/^[\d]{1,}$/";
		if(preg_match($pattern, $this->post[$field]) !== 1)
     	{
     		$label = $this->label($field);
     		$this->setError($field, "Enter a valid $label");
     	}//endif
    }
  	
    /**
  	 * length funciton
  	 * @param  string $field length validator
  	 * @return [type]        [description]
  	 */
    public function length($field)
    {
    $field = $this->post[$field];
    if($field<3 && $field>100){
     		$label = $this->label($field);
     		$this->setError($field, "Enter a valid $label have minimum length 3 and maximum length 100");
     	}//endif    
    }

	/**
	 * function to check login credentials
	 * @param  string $email_actual     -- email from database 
	 * @param  string $email_provide    -- email from login form
	 * @param  string $password_actual  -- password from database
	 * @param  string $password_provide -- password from ligin form
	 */
	public function loginCheck($email_actual, $email_provide, $password_actual, $password_provide)
	{
		if(in_array($_POST[$email_provide],$email_actual) && in_array($_POST[$password_provide],$password_actual)){
			header('Location:profile.php');
			die();
		}
		else{
			$this->setError($email_actual, "Sorry, Login credentials doesn't match");
		}
	}

  /**
   * cardno check
   * @param  integer $field 
   * @return none   */
  public function cardCheck($field)
  {
    $pattern = "/(^[\d]{16})$/";
    if(preg_match($pattern, $this->post[$field]) !== 1)
      {
        $label = $this->label($field);
        $this->setError($field, "Enter a valid $label");
      }//endif
    }

  /**
   * cvv check
   * @param  integer $field 
   * @return none   */
  public function cvvCheck($field)
  {
    $pattern = "/(^[\d]{3})$/";
    if(preg_match($pattern, $this->post[$field]) !== 1)
      {
        $label = $this->label($field);
        $this->setError($field, "Enter a valid $label");
      }//endif
    }

  /**
   * expiry_date check
   * @param  integer $field 
   * @return none   */
  public function date($field)
  {
    $pattern = "/(^[\d]{2}\/[\d]{4}$)/";
    if(preg_match($pattern, $this->post[$field]) !== 1)
      {
        $label = $this->label($field);
        $this->setError($field, "Enter a valid $label");
      }//endif
    }

    /**
     * Fet validation erros
     * @return Array -- array of errors
     */
	public function errors()
	{
		return $this->errors;
	}//errors function end

	/**
	 * Setting the error message for form field
	 * @param String --$field   value for form field
	 * @param String --$message error message
	 */
	protected function setError($field, $message)
	{
		if(empty($this->errors[$field])){
			$this->errors[$field] = $message;
		}//endif
	}// set error function end


    /**
     * Create a label from a string
     * @param  String -- $string for example, a db table field name
     * @return String -- replaing the underscore with space
     */
    protected function label($string) 
    {
        // replace underscores with a space
        // uppercase each word
        // return the result 
        return ucwords( str_replace('_', ' ', $string) ); 
    }// label function end

}// Validator class end
