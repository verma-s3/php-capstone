<?php

// namespace declaration
namespace App;

// Studio class created
class OrderModel extends Model
{		
	/**
	 * table name
	 * @var string
	 */
	protected $table = 'orders ';

	/**
	 * key value 
	 * @var integer
	 */
	protected $key = 'order_id';

	/**
	 * key value 
	 * @var integer
	 */
	protected $price = 'price';

	/**
	 * to add data on orders table
	 * @param Interger $services_id  
	 * @param Interger $customer_id  
	 * @param string $cust_name    
	 * @param string $cust_address 
	 * @param integer $price        
	 * @param float $gst          
	 * @param float $pst          
	 * @param float $total        
	 */
	public function addOrder($services_id,$customer_id,$cust_name,$cust_address,$cust_phone,$price,$gst,$pst,$total)
	{
		$query = 'INSERT into orders
				  (services_id,customer_id,cust_name,cust_address,cust_phone,price,gst,pst,total)
				  values
				  (:services_id,:customer_id,:cust_name,:cust_address,:cust_phone,:price,:gst,:pst,:total)';
	// paramters 
		$params = array(':services_id' => $services_id,
						':customer_id' => $customer_id,
						':cust_name'=> $cust_name,
						':cust_address'=>$cust_address,
						':cust_phone'=>$cust_phone,
						':price'=>$price,
						':gst'=>$gst,
						':pst'=>$pst,
            			':total'=>$total
						);
		// preparing query
	    $stmt = static::$dbh->prepare($query);
	    // excuting query
	    $stmt->execute($params);
    
	    $target_id = static::$dbh->lastInsertId();
	    return $target_id;
	}

	/**
	 * showing all order by function
	 * @return  array database data
	 */
	public function allOrder()
	{
		// making query
		$query = "SELECT orders.*,
				  services.package_type,
				  customer.email_address
				  FROM
				  orders
				  JOIN services USING(services_id)
				  JOIN customer USING(customer_id)";

		// preparing query
		$stmt = static::$dbh->prepare($query);
		// exceuting quesy
		$stmt->execute();
		// returing the fetch data
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * dispalying only one slected record by id
	 * @param  integer $id order_id
	 * @return array     sleetced order
	 */
	public function showOrder($id)
	{
		// making query
		$query = "SELECT orders.*,
				  services.*,
				  customer.*
				  FROM
				  orders
				  JOIN services USING(services_id)
				  JOIN customer USING(customer_id)
				  Where order_id = :id";

		// parameters
		$params = array(':id' => $id);
		// preparing query
		$stmt = static::$dbh->prepare($query);
		// exceuting quesy
		$stmt->execute($params);
		// returing the fetch data
		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}
}//end of class