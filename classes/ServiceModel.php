<?php

// namespace declaration
namespace App;

// Studio class created
class ServiceModel extends Model
{	
	/**
	 * table name
	 * @var string
	 */
	protected $table = 'services';

	/**
	 * key value 
	 * @var integer
	 */
	protected $key = 'services_id';

	/**
	 * key value 
	 * @var integer
	 */
	protected $price = 'price';


	/**
	 * Adding data to the dattabse
	 * @param string $package_type       
	 * @param string $session_type       
	 * @param string $photo_quality      
	 * @param string $photo_size         
	 * @param string $no_of_photos       
	 * @param string $delivery_method    
	 * @param string $price              
   * @param string $iamge              
   * @param string $photographer_name  
	 */
	public function addData($data)
	{
		$query = 'INSERT into services
				  (package_type,session_type,photo_quality,photo_size,no_of_photos,delivery_method,price,image,photographer_name)
				  values
				  (:package_type,:session_type,:photo_quality,:photo_size,:no_of_photos,:delivery_method,:price,:image,:photographer_name)';

		// paramters 
		$params = array(':package_type' => $data['package_type'],
						':session_type' => $data['session_type'],
						':photo_quality'=> $data['photo_quality'],
						':photo_size'=>$data['photo_size'],
						':no_of_photos'=>$data['no_of_photos'],
						':delivery_method'=>$data['delivery_method'],
						':price'=>$data['price'],
            			':image'=>$data['image'],
            			':photographer_name'=>$data['photographer_name']
						);
		// preparing query
	    $stmt = static::$dbh->prepare($query);
	    // excuting query
	    $stmt->execute($params);
    
	    return static::$dbh->lastInsertId();
	}
  
    /**
     * update function
     * @param  string $data --data for updation
     * @return interger  row count
     */
    public function update($data)
	{
		$query = 'UPDATE services SET
					  package_type = :package_type,
					  session_type = :session_type ,
					  photo_quality = :photo_quality,
					  photo_size = :photo_size,
					  no_of_photos = :no_of_photos,
					  delivery_method = :delivery_method,
					  price = :price,
					  photographer_name = :photographer_name
					  WHERE services_id = :services_id';
		// preparing query
	    $stmt = static::$dbh->prepare($query);
	    $params = array(':package_type' => $data['package_type'],
                    ':session_type' => $data['session_type'] ,
                    ':photo_quality' => $data['photo_quality'],
                    ':photo_size' => $data['photo_size'],
                    ':no_of_photos' => $data['no_of_photos'],
                    ':delivery_method' => $data['delivery_method'],
                    ':price' => $data['price'],
                    ':photographer_name' => $data['photographer_name'],
                    ':services_id'=>$data['services_id']
                    );
	    // executing query
	    $stmt->execute($params);
	    $r= $stmt->rowCount();
	}

	/**
	 * filtering data for seach data
	 * @param  string $method [description]
	 * @return string         [description]
	 */
	public function filterData($method)
	{
		$query = "SELECT * FROM services where delivery_method = $method";
		$stmt = static::$dbh->prepare($query);
		$stmt->execute();
		$services = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		return $services;
	}

	public function insertData($id)
	{
		// $query = 'INSERT into deleted_services
		// 		  (services_id,package_type,session_type,photo_quality,photo_size,no_of_photos,delivery_method,price,image,photographer_name)
		// 		  values
		// 		  (:services_id:package_type,:session_type,:photo_quality,:photo_size,:no_of_photos,:delivery_method,:price,:image,:photographer_name)';
		$query = 'INSERT into deleted_services SELECT * FROM services where services_id = :id';
		// paramters 
		$params = array(':id' => $id
						);
		// preparing query
	    $stmt = static::$dbh->prepare($query);

	    // excuting query
	    $stmt->execute($params);
    	// returing last insert id in the table
	    return static::$dbh->lastInsertId();
	}
}
