<?php

// namespace declaration
namespace App;

// Studio class created
class CommentsModel extends Model
{		
	/**
	 * table name
	 * @var string
	 */
	protected $table = 'comments';

	/**
	 * key value 
	 * @var string
	 */
	protected $key = 'user_id';
}