<?php

// namespace declaration
namespace App;

// Studio class created
class CustomerModel extends Model
{		
	/**
	 * table name
	 * @var string
	 */
	protected $table = 'customer';

	/**
	 * key value 
	 * @var string
	 */
	protected $key = 'customer_id';
}