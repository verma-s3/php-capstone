<?php

// namespace declaration
namespace App;

// Model class created
class Model
{
	/**
	 * $dbh cariable as protected created
	 * @var String
	 */
	protected static $dbh;

	/**
	 * Intialize databse handle for all models
	 */
	public static function init(\PDO $dbh)
	{
		static::$dbh = $dbh;
	}

	/**
	 * Resturn al ll the sreults from model table
	 * @return [type] [description]
	 */
	public function all()
	{
		$query = "SELECT * from {$this->table}";
		$stmt = static::$dbh->prepare($query);
		$stmt->execute();
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);		
	}

	/**
	 * function one is created for getting id
	 * @param  string $id 
	 * @return fecthed queries
	 */
	public function one($id)
	{
		$query = "SELECT * from {$this->table} where {$this->key} = :id";
		$params = array(':id' => $id);
		$stmt = static::$dbh->prepare($query);
		$stmt->execute($params);
		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	/**
	 * count function 
	 * @return number
	 */
	public function count()
	{
		$query = "SELECT COUNT({$this->key}) as count FROM {$this->table}";
		$stmt = static::$dbh->prepare($query);
		$stmt->execute();
		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	/**
	 * maximum function 
	 * @return number
	 */
	public function max()
	{
		$query = "SELECT MAX({$this->price}) as max FROM {$this->table}";
		$stmt = static::$dbh->prepare($query);
		$stmt->execute();
		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	/**
	 * minimum function 
	 * @return number
	 */
	public function min()
	{
		$query = "SELECT MIN({$this->price}) as min FROM {$this->table}";
		$stmt = static::$dbh->prepare($query);
		$stmt->execute();
		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	/**
	 * average function 
	 * @return number
	 */
	public function avg()
	{
		$query = "SELECT round(AVG({$this->price}),2) as avg FROM {$this->table}";
		$stmt = static::$dbh->prepare($query);
		$stmt->execute();
		return $stmt->fetch(\PDO::FETCH_ASSOC);
	}

	

}