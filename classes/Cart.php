<?php
// namespace declaration
namespace App;

// Cart class created
class Cart
{
	// defining constant valur for gst and pst 
	const GST = 0.08;
	const PST = 0.05;

	/**
	 * cart variable public
	 * @var string
	 */
	public $cart;

	/**
	 * constructor function to check empty cart 
	 */
    public function __construct()
    {
	  	// work automatically when object created
	}// end of constructor function

	/**
	 * check for cart exists or not
	 * @return true or false
	 */
	public function cartExist()
	{
		// if count is zero then return false 
		if(!count($this->cart))
		{
			return false;
		}
		// otherwise return true
		return true;
	}

	/**
	 * add the item to cart
	 * @param string $item [description]
	 */
	public function addItem($item)
	{
		// add item to cart
		$this->cart[$item['services_id']] = $item;
	}

	/**
	 * getting the subtotal
	 * @param  integer $price [description]
	 * @return $subtotal interger
	 */
	public function getSubTotal($price)
	{
		return $price	;
	}

	/**
	 * gst calculator
	 * @param  integer $price [description]
	 * @return gst
	 */
	public function gst($price)
	{
		$gst = $this->getSubTotal($price) * static::GST;
		return $gst;
	}

	/**
	 * pst calculator
	 * @param  integer $price [description]
	 * @return pst
	 */
	public function pst($price)
	{
		$pst = $this->getSubTotal($price) * static::PST;
		return $pst;
	}

	/**
	 * total calculator
	 * @param  integer $price [description]
	 * @return total value of item
	 */
	public function getTotal($price)
	{
		$subtotal = $this->getSubTotal($price);
		$gst = $this->gst($price);
		$pst = $this->pst($price);
		$total = $subtotal + $gst + $pst;
		return $total;
	}


}// end of cart class
?>