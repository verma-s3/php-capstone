<?php 


/**
 * Accept class name, convert to both path, require file
 * @param  string
 */

spl_autoload_register(function ($class)
{  
  	//Project base namespaces
  	$prefix = "App\\";

  	// base directory where my classes reside
  	$base_dir = __DIR__.'/classes/';

  	// get the length of prefix
  	$len = strlen($prefix);
    
  	// test that class name passed in is using the prefix
  	if(strncmp($prefix, $class, $len)!==0){
  		return;
  	}
    
    // Get the class minus the
  	$sub_class = substr($class, $len);

    $sub_class= str_replace('\\', '/', $sub_class);
   
  	$file = $base_dir.$sub_class.'.php';
  	

  	if(file_exists($file)){
  		require $file;
  	}



});

