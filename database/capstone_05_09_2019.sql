-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 13, 2019 at 02:42 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `photostudio`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `comments` varchar(255) NOT NULL,
  `response` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`user_id`, `first_name`, `email_address`, `comments`, `response`, `created_at`) VALUES
(10, 'sonia', 'sonia@gmail.com', 'question about packages', 0, '2019-09-05 10:36:27'),
(11, 'sonia', 'soniaverma@gmail.com', 'question about packages', 0, '2019-09-05 10:36:27'),
(12, 'Nisha', 'nisha@gmail.com', 'you  are doing great work!', 1, '2019-09-11 16:45:02'),
(13, 'gurjot', 'gurjot56@gmail.com', 'what is the price for baby shoot', 1, '2019-09-11 16:46:10'),
(14, 'Iqbal', 'iqbal34sharma@gmail.com', 'Timimgs for portarit shoot ', 1, '2019-09-11 16:46:54');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY `email_address` (`email_address`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `first_name`, `last_name`, `street`, `city`, `postal_code`, `province`, `country`, `phone`, `email_address`, `password`, `admin`, `created_at`, `updated_at`) VALUES
(45, 'sonia', 'verma', '230 jefferson ave', 'Winnipeg', 'R2P0S3', 'Manitoba', 'Canada', '123-253-4523', 'sonia@verma.com', 'Muw09$Ssa', 0, '2019-08-18 23:39:39', '2019-08-18 23:39:39'),
(46, 'sunny', 'singh', '34 mavista ave', 'brampton ', 'B3E4R4', 'Totonto', 'Canada', '2075084563', 'sunny@gmail.com', '$Onia9489', 0, '2019-08-21 18:56:53', '2019-08-21 18:56:53'),
(47, 'sonia', 'singh', '10 jefferosn Avenue', 'Winnipeg', 'R2P0Z2', 'manitoba', 'Canada', '124-123-1254', 'soniaverma45@gmail.com', 'Muw09$Ssa', 0, '2019-08-21 19:10:32', '2019-08-21 19:10:32'),
(48, 'nisha', 'verma', '12 shepperd steet', 'winnipeg', 'e3r4t5', 'manitoba', 'canada', '123-123-4563', 'nishaverma@gmail.com', '$2y$10$KownjNZphyvxmkvsdrbsdulxNQyHl54TkFKQtl6J//TzsPqJoLNTu', 0, '2019-08-21 21:31:26', '2019-08-21 21:31:26'),
(54, 'sunny', 'verma', '230 jefferson ave', 'Winnipeg', 'R2P0S3', 'Manitoba', 'Canada', '123-113-2563', 'jiya@gmail.com', '$2y$10$q470h7Ve2zAzu88bl/RcnefdBT7O9wAipo2wKB41Ga78GQfr2L8dG', 0, '2019-08-22 17:53:18', '2019-08-22 17:53:18'),
(55, 'Sonia', 'verma', '1452 Jefferson Avenue', 'winnipeg', 'R2P0Z2', 'Manitoba', 'Canada', '4315579489', '94soniaverma@gmail.com', '$2y$10$fBEPgeU3BnszSfDnCSs7fep.Upz/N3pfRE.MVG/j.dVvab77qjW4W', 0, '2019-08-22 18:11:45', '2019-08-22 18:11:45'),
(57, 'iqbal', 'jeet', '12 shepp st', 'winnipeg', 'e3r4t5', 'maniod', 'nsdjn', '1234567890', 'iqbaljit@gmail.com', '$2y$10$8HyTkVA2jyoIJdfqx1Cqu.7GhPk9ADZd8UrqA3tqr4NPTh39kje9W', 0, '2019-08-22 18:44:22', '2019-08-22 18:44:22'),
(59, 'Sonia', 'verma', '1452 Jefferson Avenue', 'winnipeg', 'R2P0Z2', 'Manitoba', 'Canada', '4315579489', '94verma@gmail.com', '$2y$10$CURzpuHpExo8zC9y1c/JTe18TPgPvuBrsPar020QlmKvVUduj3q5S', 0, '2019-08-22 19:18:53', '2019-08-22 19:18:53'),
(60, 'Sonia', 'verma', '1452 Jefferson Avenue', 'winnipeg', 'R2P0Z2', 'Manitoba', 'Canada', '4315579489', 'jashan@gmail.com', '$2y$10$7NRmDPyuSGJrpcZrEneFWO5eWFf3Je.aoH37CnezAsSX3w8P7YdjO', 0, '2019-08-22 19:49:54', '2019-08-22 19:49:54'),
(61, 'sapna', 'verma', '15 Adsum Kt', 'Winnipeg', 'R3T5Y6', 'Punjab', 'INDA', '9988201236', 'sapnaverma@gmail.com', '$2y$10$iDWkKJSfADtZix/RCXlY9eLxFLiFid7aha8osE0aRomJQP6moSP5W', 0, '2019-09-09 11:32:10', '2019-09-09 11:32:10'),
(62, 'sunny', 'verma', '12 portage ave', 'winnipeg', 'R4T 5Y6', 'Mnaitoba', 'Canada', '124-567-8920', 'sonia@gm.com', '$2y$10$AURPuoJcZCHdC5xLpcGKdekcMA8rj5wzpeGHGpxH..2w9F8sYNKqK', 0, '2019-09-10 11:41:55', '2019-09-10 11:41:55'),
(63, 'Admin', 'Admin', '56 Edmonton Street', 'Winnipeg', 'R4R 5T8', 'Manitoba', 'Canada', '431-557-9489', 'admin@gmail.com', '$2y$10$Cc.5zSe9mUBLrNcXJ/yEyuMowGu44IV9SNSbhCy6J/IBDbTcTRGjO', 1, '2019-09-11 11:47:37', '2019-09-11 11:47:37');

-- --------------------------------------------------------

--
-- Table structure for table `deleted_services`
--

DROP TABLE IF EXISTS `deleted_services`;
CREATE TABLE IF NOT EXISTS `deleted_services` (
  `services_id` int(11) NOT NULL,
  `package_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_quality` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_of_photos` int(10) NOT NULL,
  `delivery_method` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `availability` tinyint(1) NOT NULL,
  `photographer_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `deleted_services`
--

INSERT INTO `deleted_services` (`services_id`, `package_type`, `session_type`, `photo_quality`, `photo_size`, `no_of_photos`, `delivery_method`, `price`, `image`, `availability`, `photographer_name`, `created_at`, `updated_at`) VALUES
(2, 'Post-wedding Photography', '20 Hours', '48 MP', '16*20', 150, 'Cds', '$1199', 'postwedding.jpg', 1, 'Sunny Singh', '2019-09-01 18:58:24', '2019-09-01 18:58:24'),
(1, 'Pre-wedding Photography', '8 hours', '48 MP', '16*20', 500, 'Hard Disk', '$1129', 'pre-wedding.jpg', 1, 'Sonia Verma', '2019-09-01 18:45:35', '2019-09-01 18:45:35'),
(1, 'Pre-wedding Photography', '8 hours', '48 MP', '16*20', 500, 'Hard Disk', '$1129', 'pre-wedding.jpg', 1, 'Sonia Verma', '2019-09-01 18:45:35', '2019-09-01 18:45:35'),
(11, 'Night Photography', '3 hours', '48 MP', '8*10', 60, 'Hard Disk', '$1299', 'night.jpg', 1, 'Swati', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(16, 'Sunrise-Sunset Photography', '3 hours', '36 MP', '11*14', 45, 'Album', '$199', 'sun.jpg', 1, 'Evgheni', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(3, 'Wedding Photography', '24 hours', '52 MP', '11*16', 200, 'Google Drive', '$2299', 'wedding.jpg', 1, 'Gondim', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(3, 'Wedding Photography', '24 hours', '52 MP', '11*16', 200, 'Google Drive', '$2299', 'wedding.jpg', 1, 'Gondim', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(2, 'Post-wedding Photography', '20 Hours', '48 MP', '16*20', 150, 'Cds', '$1199', 'postwedding.jpg', 1, 'Sunny Singh', '2019-09-01 18:58:24', '2019-09-01 18:58:24'),
(1, 'Pre-wedding Photography', '8 hours', '48 MP', '16*20', 500, 'Hard Disk', '$1129', 'pre-wedding.jpg', 1, 'Sonia Verma', '2019-09-01 18:45:35', '2019-09-01 18:45:35'),
(1, 'Pre-wedding Photography', '32 hours', '48 MP', '16*20', 500, 'Hard Disk', '$1129', 'pre-wedding.jpg', 1, 'Sonia Verma', '2019-09-01 18:45:35', '2019-09-01 18:45:35'),
(1, 'Pre-wedding Photography', '32 hours', '48 MP', '16*20', 500, 'Hard Disk', '$1129', 'pre-wedding.jpg', 1, 'Sonia Verma', '2019-09-01 18:45:35', '2019-09-01 18:45:35'),
(1, 'Pre-wedding Photography', '32 hours', '48 MP', '16*20', 500, 'Hard Disk', '$1129', 'pre-wedding.jpg', 1, 'Sonia Verma', '2019-09-01 18:45:35', '2019-09-01 18:45:35'),
(1, 'Pre-wedding Photography', '32 hours', '48 MP', '16*20', 500, 'Hard Disk', '$1129', 'pre-wedding.jpg', 1, 'Sonia Verma', '2019-09-01 18:45:35', '2019-09-01 18:45:35'),
(1, 'Pre-wedding Photography', '32 hours', '48 MP', '16*20', 500, 'Hard Disk', '$1129', 'pre-wedding.jpg', 1, 'Sonia Verma', '2019-09-01 18:45:35', '2019-09-01 18:45:35'),
(22, 'Couple Photography', '2 hours', '24 MP', '11*18', 20, 'Pen Drive', '$69', 'main.jpg', 1, 'Mariam', '2019-09-12 15:24:25', '2019-09-12 15:24:25');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id`, `event`, `created_at`) VALUES
(1, '2019-09-13 01:12:52 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:12:52'),
(2, '2019-09-13 01:14:42 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:14:42'),
(3, '2019-09-13 01:14:44 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:14:44'),
(4, '2019-09-13 01:15:54 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:15:54'),
(5, '2019-09-13 01:19:45 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:19:45'),
(6, '2019-09-13 01:37:10 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:37:10'),
(7, '2019-09-13 01:40:35 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:40:36'),
(8, '2019-09-13 01:41:08 | admin/index.php?order=%27desc%27 | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:41:08'),
(9, '2019-09-13 01:45:01 | admin/index.php?order=%27desc%27 | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:45:01'),
(10, '2019-09-13 01:45:17 | admin/index.php?order=%27desc%27 | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:45:17'),
(11, '2019-09-13 01:45:40 | admin/index.php?order=%27desc%27 | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:45:40'),
(12, '2019-09-13 01:48:55 | admin/index.php?order=%27desc%27 | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:48:55'),
(13, '2019-09-13 01:49:03 | admin/index.php?order=%27desc%27 | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:49:03'),
(14, '2019-09-13 01:49:08 | admin/index.php?order=%27desc%27 | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:49:08'),
(15, '2019-09-13 01:49:13 | admin/index.php?order=%27asc%27 | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:49:13'),
(16, '2019-09-13 01:49:20 | admin/index.php?order=%27asc%27 | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:49:20'),
(17, '2019-09-13 01:52:11 | admin/index.php?order=%27asc%27 | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:52:11'),
(18, '2019-09-13 01:52:16 | admin/index.php?order=%27asc%27 | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:52:16'),
(19, '2019-09-13 01:52:18 | admin/index.php?order=%27desc%27 | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:52:18'),
(20, '2019-09-13 01:52:21 | admin/index.php?order=%27desc%27 | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:52:21'),
(21, '2019-09-13 01:52:28 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:52:28'),
(22, '2019-09-13 01:54:21 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:54:22'),
(23, '2019-09-13 01:54:48 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:54:48'),
(24, '2019-09-13 01:55:58 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:55:58'),
(25, '2019-09-13 01:56:36 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:56:36'),
(26, '2019-09-13 01:57:05 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:57:05'),
(27, '2019-09-13 01:58:19 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:58:19'),
(28, '2019-09-13 01:58:42 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:58:42'),
(29, '2019-09-13 01:58:57 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 20:58:57'),
(30, '2019-09-13 02:01:21 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:01:21'),
(31, '2019-09-13 02:03:29 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:03:29'),
(32, '2019-09-13 02:04:36 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:04:36'),
(33, '2019-09-13 02:04:37 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:04:37'),
(34, '2019-09-13 02:04:39 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:04:39'),
(35, '2019-09-13 02:04:55 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:04:55'),
(36, '2019-09-13 02:05:48 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:05:48'),
(37, '2019-09-13 02:06:45 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:06:45'),
(38, '2019-09-13 02:07:55 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:07:55'),
(39, '2019-09-13 02:09:31 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:09:31'),
(40, '2019-09-13 02:10:09 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:10:09'),
(41, '2019-09-13 02:11:10 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:11:10'),
(42, '2019-09-13 02:12:13 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:12:13'),
(43, '2019-09-13 02:12:19 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:12:19'),
(44, '2019-09-13 02:12:20 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:12:20'),
(45, '2019-09-13 02:12:22 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:12:22'),
(46, '2019-09-13 02:12:25 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:12:25'),
(47, '2019-09-13 02:12:28 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:12:28'),
(48, '2019-09-13 02:12:38 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:12:38'),
(49, '2019-09-13 02:13:06 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:13:06'),
(50, '2019-09-13 02:16:06 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:16:06'),
(51, '2019-09-13 02:16:19 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:16:19'),
(52, '2019-09-13 02:16:40 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:16:41'),
(53, '2019-09-13 02:16:47 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:16:47'),
(54, '2019-09-13 02:17:34 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:17:34'),
(55, '2019-09-13 02:17:56 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:17:56'),
(56, '2019-09-13 02:18:08 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:18:08'),
(57, '2019-09-13 02:18:58 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:18:58'),
(58, '2019-09-13 02:21:44 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:21:44'),
(59, '2019-09-13 02:21:52 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:21:52'),
(60, '2019-09-13 02:21:53 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:21:53'),
(61, '2019-09-13 02:21:53 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:21:53'),
(62, '2019-09-13 02:21:53 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:21:53'),
(63, '2019-09-13 02:21:54 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:21:54'),
(64, '2019-09-13 02:21:59 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:21:59'),
(65, '2019-09-13 02:22:00 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:22:00'),
(66, '2019-09-13 02:22:02 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:22:02'),
(67, '2019-09-13 02:22:03 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:22:03'),
(68, '2019-09-13 02:22:05 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:22:05'),
(69, '2019-09-13 02:22:06 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:22:06'),
(70, '2019-09-13 02:22:07 | admin/index.php | GET | Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0', '2019-09-12 21:22:07');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `services_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `cust_name` varchar(255) NOT NULL,
  `cust_address` varchar(255) NOT NULL,
  `cust_phone` varchar(16) NOT NULL,
  `price` int(11) NOT NULL,
  `gst` decimal(6,2) NOT NULL,
  `pst` decimal(6,2) NOT NULL,
  `total` decimal(6,2) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_date`, `services_id`, `customer_id`, `cust_name`, `cust_address`, `cust_phone`, `price`, `gst`, `pst`, `total`, `created_at`, `updated_at`) VALUES
(1, '2019-09-10 04:23:40', 6, 48, 'nisha', 'Jefferson Avenue', '1236547890', 129, '10.32', '6.45', '145.77', '2019-09-05 10:32:45', '2019-09-05 10:32:45'),
(2, '2019-06-13 20:07:48', 5, 57, 'iqbal', 'Poratage Avenue', '7984561235', 199, '15.92', '9.95', '224.87', '2019-09-05 10:32:45', '2019-09-05 10:32:45'),
(3, '2019-07-15 17:22:34', 1, 45, 'sonia', 'Adsum Drive', '136542310', 1299, '103.92', '64.95', '1467.87', '2019-09-05 10:32:45', '2019-09-05 10:32:45'),
(4, '2019-08-19 06:42:11', 14, 57, 'iqbal', 'Sherbook Avenue', '4561237985', 999, '7.99', '4.99', '1011.99', '2019-09-05 10:32:45', '2019-09-05 10:32:45'),
(5, '2019-09-25 09:00:08', 17, 54, 'sunny', 'Amber Trails', '7899511589', 129, '10.32', '6.45', '145.77', '2019-09-05 10:32:45', '2019-09-05 10:32:45'),
(6, '2019-09-10 11:37:38', 1, 61, 'sapna verma', 'Winnipeg INDA Punjab R3T5Y6', '9988201236', 1129, '90.32', '56.45', '1275.77', '2019-09-10 11:37:38', '2019-09-10 11:37:38'),
(7, '2019-09-10 11:39:31', 1, 61, 'sapna verma', 'Winnipeg Punjab INDA R3T5Y6', '9988201236', 1129, '90.32', '56.45', '1275.77', '2019-09-10 11:39:31', '2019-09-10 11:39:31'),
(8, '2019-09-10 11:42:43', 1, 62, 'sunny verma', 'winnipeg Mnaitoba Canada R4T 5Y6', '124-567-8920', 1129, '90.32', '56.45', '1275.77', '2019-09-10 11:42:43', '2019-09-10 11:42:43'),
(9, '2019-09-10 11:43:09', 1, 62, 'sunny verma', 'winnipeg Mnaitoba Canada R4T 5Y6', '124-567-8920', 1129, '90.32', '56.45', '1275.77', '2019-09-10 11:43:09', '2019-09-10 11:43:09'),
(10, '2019-09-10 11:43:47', 1, 62, 'sunny verma', 'winnipeg Mnaitoba Canada R4T 5Y6', '124-567-8920', 1129, '90.32', '56.45', '1275.77', '2019-09-10 11:43:47', '2019-09-10 11:43:47'),
(11, '2019-09-10 11:45:17', 1, 62, 'sunny verma', 'winnipeg Mnaitoba Canada R4T 5Y6', '124-567-8920', 1129, '90.32', '56.45', '1275.77', '2019-09-10 11:45:17', '2019-09-10 11:45:17'),
(12, '2019-09-10 13:18:43', 1, 61, 'sapna verma', 'Winnipeg Punjab INDA R3T5Y6', '9988201236', 1129, '90.32', '56.45', '1275.77', '2019-09-10 13:18:43', '2019-09-10 13:18:43'),
(13, '2019-09-10 13:20:05', 1, 61, 'sapna verma', 'Winnipeg Punjab INDA R3T5Y6', '9988201236', 1129, '90.32', '56.45', '1275.77', '2019-09-10 13:20:05', '2019-09-10 13:20:05'),
(14, '2019-09-10 13:21:34', 1, 61, 'sapna verma', 'Winnipeg Punjab INDA R3T5Y6', '9988201236', 1129, '90.32', '56.45', '1275.77', '2019-09-10 13:21:34', '2019-09-10 13:21:34'),
(15, '2019-09-10 13:34:52', 1, 62, 'sunny verma', 'winnipeg Mnaitoba Canada R4T 5Y6', '124-567-8920', 1129, '90.32', '56.45', '1275.77', '2019-09-10 13:34:52', '2019-09-10 13:34:52'),
(16, '2019-09-10 14:57:58', 4, 61, 'sapna verma', 'Winnipeg Punjab INDA R3T5Y6', '9988201236', 299, '23.92', '14.95', '337.87', '2019-09-10 14:57:58', '2019-09-10 14:57:58'),
(17, '2019-09-12 19:34:22', 2, 63, 'Admin Admin', 'Winnipeg Manitoba Canada R4R 5T8', '431-557-9489', 1199, '95.92', '59.95', '1354.87', '2019-09-12 19:34:22', '2019-09-12 19:34:22');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `services_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_quality` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_of_photos` int(10) NOT NULL,
  `delivery_method` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(10) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `availability` tinyint(1) NOT NULL DEFAULT '1',
  `photographer_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`services_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`services_id`, `package_type`, `session_type`, `photo_quality`, `photo_size`, `no_of_photos`, `delivery_method`, `price`, `image`, `availability`, `photographer_name`, `created_at`, `updated_at`) VALUES
(1, 'Pre-wedding Photography', '32 hours', '48 MP', '16*20', 500, 'Hard Disk', 1129, 'pre-wedding.jpg', 1, 'Sonia Verma', '2019-09-01 18:45:35', '2019-09-01 18:45:35'),
(2, 'Post-wedding Photography', '20 Hours', '48 MP', '16*20', 150, 'Cds', 1199, 'postwedding.jpg', 1, 'Sunny Singh', '2019-09-01 18:58:24', '2019-09-01 18:58:24'),
(3, 'Wedding Photography', '24 hours', '52 MP', '11*16', 200, 'Google Drive', 2299, 'wedding.jpg', 1, 'Gondim', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(4, 'New Born Photography', '2 hours', '24 MP', '11*14', 200, 'Album', 299, 'new-born.jpg', 1, 'Katie', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(5, 'Portfolio Photography', '4 hours', '52 MP', '9*16', 150, 'Pen Drive', 199, 'protfolio.jpg', 1, 'Nic Johnes', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(6, 'Family Photography', '1 hours', '36 MP', '12*20', 120, 'Prints', 129, 'family.jpg', 1, 'Swati', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(7, 'Birthday Photography', '2 hours', '24 MP', '5*7', 100, 'Google Drive', 399, 'birthday.jpg', 1, 'Gondim', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(8, 'Baby Photography', '1 hours', '32 MP', '16*20', 70, 'Album', 179, 'baby.jpg', 1, 'Karen', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(9, 'Event Photography', '6 hours', '20 MP', '8*8', 100, 'Pen Drive', 899, 'event.jpg', 1, 'Katie', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(10, 'Modeling Photography', '2 hours', '100 MP', '11*16', 40, 'Google Drive', 1099, 'modeling.jpg', 1, 'Suzaane', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(11, 'Night Photography', '3 hours', '48 MP', '8*10', 60, 'Hard Disk', 1299, 'night.jpg', 1, 'Swati', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(12, 'Rain Photography', '2 hours', '48 MP', '4*6', 25, 'Album', 1099, 'rain.jpg', 1, 'Sonia Verma', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(13, 'Real Estate Photography', '5 hours', '36 MP', '9*16', 60, 'Prints', 699, 'real-estate.jpg', 1, 'Sunny Singh', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(14, 'Sports Photography', '8 hours', '42 MP', '12*18', 200, 'CDs', 999, 'sports.jpg', 1, 'Maheer', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(15, 'Fashion  Photography', '12 hours', '48 MP', '16*20', 100, 'Google Drive', 399, 'fashion.jpg', 1, 'Suzaane', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(16, 'Sunrise-Sunset Photography', '3 hours', '36 MP', '11*14', 45, 'Album', 199, 'sun.jpg', 1, 'Evgheni', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(17, 'Time Lapse Photography', '1 hours', '24 MP', '11*16', 1, 'Cds', 129, 'time.jpg', 10, 'Karen', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(18, 'Underwater Photography', '2 hours', '28 MP', '16*20', 30, 'Pen Drive', 1089, 'underwater.jpg', 1, 'Maheer', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(19, 'Wildlife Photography', '3 hours', '36 MP', '8*10', 20, 'Prints', 259, 'wild.jpg', 1, 'Evgheni', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(20, 'Still life Photography', '4 hours', '42 MP', '5*7', 50, 'Hard Disk', 599, 'stilllife.jpg', 1, 'Nic Johnes', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(21, 'Couple Photography', '2 hours', '24 MP', '11*18', 20, 'Pen Drive', 69, 'main.jpg', 1, 'Mariam', '2019-09-12 15:23:57', '2019-09-12 15:23:57');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
