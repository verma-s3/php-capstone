-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 05, 2019 at 04:47 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `photostudio`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `comments` varchar(255) NOT NULL,
  `response` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`user_id`, `first_name`, `email_address`, `comments`, `response`, `created_at`) VALUES
(1, 'sonia', 'sonia@gmail.com', 'question about packages', 0, '2019-09-05 10:36:27'),
(2, 'sonia', 'sonia@gmail.com', 'question about packages', 0, '2019-09-05 10:36:27'),
(3, 'sonia', 'sonia@gmail.com', 'question about packages', 0, '2019-09-05 10:36:27'),
(4, 'sonia', 'sonia@gmail.com', 'question about packages', 0, '2019-09-05 10:36:27'),
(5, 'sonia', 'sonia@gmail.com', 'question about packages', 0, '2019-09-05 10:36:27'),
(6, 'sonia', 'sonia@gmail.com', 'question about packages', 0, '2019-09-05 10:36:27'),
(7, 'sonia', 'sonia@gmail.com', 'question about packages', 0, '2019-09-05 10:36:27'),
(8, 'sonia', 'sonia@gmail.com', 'question about packages', 0, '2019-09-05 10:36:27'),
(9, 'sonia', 'sonia@gmail.com', 'question about packages', 0, '2019-09-05 10:36:27'),
(10, 'sonia', 'sonia@gmail.com', 'question about packages', 0, '2019-09-05 10:36:27'),
(11, 'sonia', 'soniaverma@gmail.com', 'question about packages', 0, '2019-09-05 10:36:27');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY `email_address` (`email_address`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customer_id`, `first_name`, `last_name`, `street`, `city`, `postal_code`, `province`, `country`, `phone`, `email_address`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Sonia', '', '', '', '', '', '', '0', 'soniaverma@gmail.com', 'soniaverma23', '2019-08-08 11:36:59', '2019-08-08 11:37:01'),
(2, 'Nisha', '', '', '', '', '', '', '0', 'nishaverma98@gmail.com', 'nisha34verma', '2019-08-08 11:36:59', '2019-08-08 11:37:01'),
(3, 'Manpreet Singh', '', '', '', '', '', '', '0', 'manpreetsingh@gmail.com', 'manpreetsadiora', '2019-08-08 11:36:59', '2019-08-08 11:37:01'),
(4, 'Evegheni', '', '', '', '', '', '', '0', 'evegheni@gmail.com', 'eveghenimanager', '2019-08-08 11:36:59', '2019-08-08 11:37:01'),
(5, 'Gondim', '', '', '', '', '', '', '0', 'gondimbrazil@gmail.com', 'gondimpass', '2019-08-08 11:36:59', '2019-08-08 11:37:01'),
(45, 'sonia', 'verma', '230 jefferson ave', 'Winnipeg', 'R2P0S3', 'Manitoba', 'Canada', '123-253-4523', 'sonia@verma.com', 'Muw09$Ssa', '2019-08-18 23:39:39', '2019-08-18 23:39:39'),
(46, 'sunny', 'singh', '34 mavista ave', 'brampton ', 'B3E4R4', 'Totonto', 'Canada', '2075084563', 'sunny@gmail.com', '$Onia9489', '2019-08-21 18:56:53', '2019-08-21 18:56:53'),
(47, 'sonia', 'singh', '10 jefferosn Avenue', 'Winnipeg', 'R2P0Z2', 'manitoba', 'Canada', '124-123-1254', 'soniaverma45@gmail.com', 'Muw09$Ssa', '2019-08-21 19:10:32', '2019-08-21 19:10:32'),
(48, 'nisha', 'verma', '12 shepperd steet', 'winnipeg', 'e3r4t5', 'manitoba', 'canada', '123-123-4563', 'nishaverma@gmail.com', '$2y$10$KownjNZphyvxmkvsdrbsdulxNQyHl54TkFKQtl6J//TzsPqJoLNTu', '2019-08-21 21:31:26', '2019-08-21 21:31:26'),
(54, 'sunny', 'verma', '230 jefferson ave', 'Winnipeg', 'R2P0S3', 'Manitoba', 'Canada', '123-113-2563', 'jiya@gmail.com', '$2y$10$q470h7Ve2zAzu88bl/RcnefdBT7O9wAipo2wKB41Ga78GQfr2L8dG', '2019-08-22 17:53:18', '2019-08-22 17:53:18'),
(55, 'Sonia', 'verma', '1452 Jefferson Avenue', 'winnipeg', 'R2P0Z2', 'Manitoba', 'Canada', '4315579489', '94soniaverma@gmail.com', '$2y$10$fBEPgeU3BnszSfDnCSs7fep.Upz/N3pfRE.MVG/j.dVvab77qjW4W', '2019-08-22 18:11:45', '2019-08-22 18:11:45'),
(57, 'iqbal', 'jeet', '12 shepp st', 'winnipeg', 'e3r4t5', 'maniod', 'nsdjn', '1234567890', 'iqbaljit@gmail.com', '$2y$10$8HyTkVA2jyoIJdfqx1Cqu.7GhPk9ADZd8UrqA3tqr4NPTh39kje9W', '2019-08-22 18:44:22', '2019-08-22 18:44:22'),
(59, 'Sonia', 'verma', '1452 Jefferson Avenue', 'winnipeg', 'R2P0Z2', 'Manitoba', 'Canada', '4315579489', '94verma@gmail.com', '$2y$10$CURzpuHpExo8zC9y1c/JTe18TPgPvuBrsPar020QlmKvVUduj3q5S', '2019-08-22 19:18:53', '2019-08-22 19:18:53'),
(60, 'Sonia', 'verma', '1452 Jefferson Avenue', 'winnipeg', 'R2P0Z2', 'Manitoba', 'Canada', '4315579489', 'jashan@gmail.com', '$2y$10$7NRmDPyuSGJrpcZrEneFWO5eWFf3Je.aoH37CnezAsSX3w8P7YdjO', '2019-08-22 19:49:54', '2019-08-22 19:49:54');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` datetime NOT NULL,
  `order_no` int(10) NOT NULL,
  `services_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `cust_address` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `gst` decimal(6,2) NOT NULL,
  `pst` decimal(6,2) NOT NULL,
  `total` decimal(6,2) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`order_id`),
  KEY `customer_id` (`customer_id`),
  KEY `services_id` (`services_id`,`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_date`, `order_no`, `services_id`, `customer_id`, `cust_address`, `price`, `qty`, `subtotal`, `gst`, `pst`, `total`, `created_at`, `updated_at`) VALUES
(1, '2019-09-10 04:23:40', 1, 6, 48, 'Jefferson Avenue', 129, 1, 129, '10.32', '6.45', '145.77', '2019-09-05 10:32:45', '2019-09-05 10:32:45'),
(2, '2019-06-13 20:07:48', 2, 5, 57, 'Poratage Avenue', 199, 1, 199, '15.92', '9.95', '224.87', '2019-09-05 10:32:45', '2019-09-05 10:32:45'),
(3, '2019-07-15 17:22:34', 3, 1, 45, 'Adsum Drive', 1299, 1, 1299, '103.92', '64.95', '1467.87', '2019-09-05 10:32:45', '2019-09-05 10:32:45'),
(4, '2019-08-19 06:42:11', 4, 14, 57, 'Sherbook Avenue', 999, 1, 999, '7.99', '4.99', '1011.99', '2019-09-05 10:32:45', '2019-09-05 10:32:45'),
(5, '2019-09-25 09:00:08', 5, 17, 54, 'Amber Trails', 129, 1, 129, '10.32', '6.45', '145.77', '2019-09-05 10:32:45', '2019-09-05 10:32:45');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `services_id` int(11) NOT NULL AUTO_INCREMENT,
  `package_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_quality` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_of_photos` int(10) NOT NULL,
  `delivery_method` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `availability` tinyint(1) NOT NULL,
  `photographer_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`services_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`services_id`, `package_type`, `session_type`, `photo_quality`, `photo_size`, `no_of_photos`, `delivery_method`, `price`, `image`, `availability`, `photographer_name`, `created_at`, `updated_at`) VALUES
(1, 'Pre-wedding Photography', '32 hours', '48 MP', '16*20', 500, 'Album', '$1299', 'pre-wedding.jpg', 1, 'Sonia Verma', '2019-09-01 18:45:35', '2019-09-01 18:45:35'),
(2, 'Pre-wedding Photography', '20 Hours', '48 MP', '16*20', 150, 'Pen Drive', '$1199', 'postwedding.jpg', 1, 'Sunny Singh', '2019-09-01 18:58:24', '2019-09-01 18:58:24'),
(3, 'Wedding Photography', '24 hours', '52 MP', '11*16', 200, 'Google Drive', '$2299', 'wedding.jpg', 1, 'Gondim', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(4, 'New Born Photography', '2 hours', '24 MP', '11*14', 200, 'Album', '$299', 'new_born.jpg', 1, 'Katie', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(5, 'Portfolio Photography', '4 hours', '52 MP', '9*16', 150, 'Pen Drive', '$199', 'protfolio.jpg', 1, 'Nic Johnes', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(6, 'Family Photography', '1 hours', '36 MP', '12*20', 120, 'Album', '$129', 'family.jpg', 1, 'Swati', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(7, 'Birthday Photography', '2 hours', '24 MP', '5*7', 120, 'Google Drive', '$399', 'birthday.jpg', 1, 'Gondim', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(8, 'Baby Photography', '1 hours', '32 MP', '16*20', 70, 'Album', '$179', 'baby.jpg', 1, 'Karen', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(9, 'Event Photography', '6 hours', '20 MP', '8*8', 100, 'Pen Drive', '$899', 'event.jpg', 1, 'Katie', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(10, 'Modeling Photography', '2 hours', '100 MP', '11*16', 40, 'Google Drive', '$1099', 'modeling.jpg', 1, 'Suzaane', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(11, 'Night Photography', '3 hours', '48 MP', '8*10', 60, 'Pen Drive', '$1299', 'night.jpg', 1, 'Swati', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(12, 'Rain Photography', '2 hours', '48 MP', '4*6', 25, 'Album', '$1099', 'rain.jpg', 1, 'Sonia Verma', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(13, 'Real Estate Photography', '5 hours', '36 MP', '9*16', 60, 'Pen Drive', '$699', 'real_estate.jpg', 1, 'Sunny Singh', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(14, 'Sports Photography', '8 hours', '42 MP', '12*18', 200, 'Album', '$999', 'sports.jpg', 1, 'Maheer', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(15, 'Fashion  Photography', '12 hours', '48 MP', '16*20', 100, 'Google Drive', '$399', 'fashion.jpg', 1, 'Suzaane', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(16, 'Sunrise/Sunset Photography', '3 hours', '36 MP', '11*14', 45, 'Album', '$199', 'sun.jpg', 1, 'Evgheni', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(17, 'Time Lapse Photography', '1 hours', '24 MP', '11*16', 1, 'Google Drive', '$129', 'time.jpg', 10, 'Karen', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(18, 'Underwater Photography', '2 hours', '28 MP', '16*20', 30, 'Pen Drive', '$1089', 'underwater.jpg', 1, 'Maheer', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(19, 'Wildlife Photography', '3 hours', '36 MP', '8*10', 20, 'Album', '$259', 'portrait.jpg', 1, 'Evgheni', '2019-09-01 18:59:25', '2019-09-01 18:59:25'),
(20, 'Still life Photography', '4 hours', '42 MP', '5*7', 50, 'Google Drive', '$599', 'stilllife.jpg', 1, 'Nic Johnes', '2019-09-01 18:59:25', '2019-09-01 18:59:25');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`services_id`) REFERENCES `services` (`services_id`),
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
