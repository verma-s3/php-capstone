<?php 
  
  require __DIR__.'/../config.php';
  /**
   * About Page 
   * last_update: 2019-08-02
   * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
   * Site name : Khera Digital Studio and Color Lab
   */
  $title = "About";
  // adding main head file
  require __DIR__.'/../inc/head.inc.php'; 
?>
  
  <body>
   
    <!-- header PHP file -->
    <?php 

    // header file included
    require __DIR__.'/../inc/header.inc.php'; 
    ?>
    
    <section>
      <div id="container"><!-- container div id starting -->
        <div id="inner"><!-- inner div starting -->
          <h1><span class="underline">About Us</span></h1>
          <div id="owner_details"><!-- owner_details div starting -->
            <div class="img-box"><!-- img-box class starting -->
              <img src="Images/dp1.jpg" alt="photo" />
              <div class="overlay"><!-- overlay class starting  -->
                <div class="text">Khera Studio</div>
              </div><!-- overlay class ending -->
            </div><!-- img-box class ending -->
            <p style="margin-left: 40px; margin-right: 40px;">
              Mani is  the founder of Khera Lab And Digital Studio. He is very passoinate for his work. He is 
              man of few words. His work speaks for himself rather than his thoughts. He started his career at
              the age of 21. He is well known photograher in the punjab. what is unique about him is his photography
              skills. His passion for photography has always made him strong to work in hectic and hard schedule. 
            </p>
          </div><!-- owner_details div ending -->
          <hr />  
          <div id="about_content"><!-- about_content div starting -->
            <img src="Images/about_pic.jpg" alt="photo"/>
            <h2 style="color: #900; text-align: center; padding-top: 10px; margin-bottom: 0;">
                Khera Color Lab and Digital Studio
            </h2>
            <p >
              At Khera Color lab and Digital Studio, we transform dreams into reality,
              by printing them directly to any surface or canvas. Specialized in manufacturing
              of photo albums, designer apple albums, pillow printing, mug printing, digital 
              printing, calender making, and much more, we provide all high resolution photos
              printing solutions. Our entire range of printing services and unique collection of
              gallery products, make us the prior choice of our market. Right from small album 
              size photographs to large wall size prints and canvas, from photo albums to surface 
              printing, we expertise in all. All latest machines and our quality work speaks
              itself for what we are0.
              <br/>
              <br/>

              With over Years of experience, we are keen about the vision of an individual to create
              a lot more memories with us. Our photographers understand well about the needs and
              vision of customer to create those excellent moments and beading them into the thread of 
              happiness to make a record of ever-lasting moments at just a place which reminds their 
              mind onto those wonderful moments they made during their wedding or an event. With 
              pride we are pleased to share our client acclamation towards our work for its on-time
              delivery, quality, creativity and uniquity. 
            </p>
          </div><!-- about content div ending -->
        </div><!-- inner div ending -->
      </div><!-- container div ending -->
    </section>
    
    <!-- Footer PHP File -->
    <?php 
    // adding footer file 
    require __DIR__.'/../inc/footer.inc.php'; 
    ?>  
  </body>
</html>