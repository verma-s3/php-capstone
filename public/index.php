<?php 
  require __DIR__.'/../config.php';
  /**
   * Home Page 
   * last_update: 2019-08-02
   * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
   * Site name : Khera Digital Studio and Color Lab
   */
  $title = "Home";
  
  // adding main head file
  require __DIR__.'/../inc/head.inc.php';  
?>
  
  <body>
   
    <!-- header PHP file -->
    <?php
    // adding header file
    require __DIR__.'/../inc/header.inc.php'; 
    ?>
    
    <section>
      <div id="container"><!-- div container starting -->
        <div id="inner"><!-- div ending -->
          <div><img src="Images/main.jpg" alt="cover_image"  /></div>
          <div id="welcome_content"><!-- welcome_content div starting -->
            <h1>Welcome</h1>
            <p>
              Welcome to Khera Color Lab and Digital Studio, moga's best photography and videography service
              dedicated to creating lasing memories for years to come. Our expertise team of photographers, 
              cinematographers, and artists do the work with great enthusiasm. Our proficient team of photographers
              always ready to take the creativity at par with their passion. We specialized in wedding shoots, 
              engagement shoots, pre-wedding and post-wedding shoots, martinity shoots, baby shoots, portfolios,
              and many more. Apart from this we provide the other services like apple album, wedding box and cards, 
              personalised gifts, etc. 
            </p>
          </div><!-- welcome_content div ending -->
          <div id="info_content"><!-- info_content div starting -->
            <img src="Images/front_page.jpg" alt="photo" />
            <h2>What We Do</h2>
            <p>
              We offer wedding phototgraphy, cinematography, ecommerce photography pre-wedding and 
              post-wedding shoots and many more.
            </p>

            <h2>What We Love</h2>
            <p>
              We embrace capturing real moments of love, affection and joy our artistic skills, play
              a vital role in having perfect pictures.
            </p>

            <h2>What We believe in</h2>
            <p>
              Photography is the combination of science and art and only experts can balance these two 
              vital spheres when it comes to capture moments. 
            </p>
          </div><!-- info_content div ending -->
          <div id="gallery"><!-- gallery div starting --> 
            <p>
              <span class="underline">Photo Gallery</span>
            </p>
          </div><!-- gallery div ending -->
          <div id="pic" ><!-- pic div starting -->
            <img src="Images/pic1.jpg" alt="pic1" />
            <img src="Images/pic2.jpg" alt="pic2" />
            <img src="Images/pic3.jpg" alt="pic3" />
            <img src="Images/pic4.jpg" alt="pic4" />
          </div><!-- pic div ending -->
          <div id="photo_link"><!-- photo_link div starting -->
            <a href="photo.php">View more</a>
          </div><!-- photo_link div ending -->
        </div><!-- div inner ending -->
      </div><!-- div container ending -->
    </section>
  
    <!-- Footer PHP File -->
    <?php 
    // adding footer file 
    require __DIR__.'/../inc/footer.inc.php'; 
    ?> 
  </body>
</html>