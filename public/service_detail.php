<?php 
  require __DIR__.'/../config.php';
  /**
   * Service Page 
   * last_update: 2019-08-02
   * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
   * Site name : Khera Digital Studio and Color Lab
   */
  
  require __DIR__.'/../autoloaded.php';
  $title = "Service detail";

  // using services from models
  use App\ServiceModel;
  
  // instantiating the object
  $s = new ServiceModel;

  //check for reuest for id
  if('GET' == $_SERVER['REQUEST_METHOD']){
    if(isset($_GET['id'])){
      $id = $_GET['id'];
      $services = $s->one($id);
    }
  }
  
  
  //cjeck for item added to cart
  if('POST' == $_SERVER['REQUEST_METHOD']){
      $id = $_REQUEST['services_id'];
      $services = $s->one($id);
      $_SESSION['cart_item'] = $id;
  }



  // adding main head file
  require __DIR__.'/../inc/head.inc.php';


?>
  
  <body>
   
    <!-- header PHP file -->
    <?php
    // adding header file
    require __DIR__.'/../inc/header.inc.php'; 
    ?>
    
    <section>
      <div id="container"><!-- container div started -->
        <div id="inner"><!-- inner div started -->
          <a class="back" href="services.php" title="service page">Back to Services</a>

          <!-- cart information -->
          <?php if(!empty($id)) : ?>
          <div id="cart">
            <div id ="cart_info">
              <?php if(empty($_SESSION['cart_item'])) : ?>
                  <p>Your Cart is empty. <br/>No service is booked yet.</p>
              <?php else : ?>
                  <p>Your booked service is: <span style="color:#ff0;"><?=esc($services['package_type']) ?></span><br /><br/>
                  <a class="add_cart" href="addtocart.php?id=<?=esc_attr($id)?>">View Cart</a></p>
              <?php endif; ?>
            </div>            
          </div><!-- /cart -->

          <!-- package detailed information -->
          <h1><?=$services['package_type']?></h1>
          <div class="item">
              <div class="img">
                <img src="Images/<?=esc_attr($services['image'])?>" alt="pics"/>
              </div>
              <div class="details">
                <h2>Details</h2>
                <table>
                  <tr>
                    <td><span>Photo quality</span></td>
                    <td><?=esc($services['photo_quality'])?></td>
                  </tr>
                  <tr>
                    <td><span>Session time</span></td>
                    <td><?=esc($services['session_type'])?></td>
                  </tr>
                  <tr>
                    <td><span>Photo size</span></td>
                    <td><?=esc($services['photo_size'])?></td>
                  </tr>
                  <tr>
                    <td><span>Photo quantity</span></td>
                    <td><?=esc($services['no_of_photos'])?></td>
                  </tr>
                  <tr>
                    <td><span>Delivery method</span></td>
                    <td><?=esc($services['delivery_method'])?></td>
                  </tr>
                  <tr>
                    <td><span>Photographer Name</span></td>
                    <td><?=esc($services['photographer_name'])?></td>
                  </tr>
                  <tr>
                    <td><span>Available</span></td>
                    <td>
                      <!-- condition to check availabitltiy -->
                    <?php 
                      if(esc($services['availability'])==1)
                      {
                        echo "Yes";
                      }
                      else{
                        echo "No";
                      }
                    ?>
                    </td>
                  </tr>
                  <tr>
                    <td><span>Price</span></td>
                    <td>$<?=esc($services['price'])?></td>
                  </tr>
                  <tr>
                  </tr>
                </table>
                <form method="post" action="service_detail.php">
                  <!-- cref protection -->
                  <input type="hidden" name="csrf" value="<?=esc_attr($_SESSION['csrf'])?>" />
                  <!-- posting hidden services id -->
                  <input type="hidden" name="services_id"
                         value="<?=esc_attr($services['services_id'])?>" 
                         />
                  <input type="submit" name="cart" value="Book Your Service Now" class="search-btn">
                </form>
              </div>             
            </div>
          <?php else : ?>
            <?php 
            $_SESSION['message'] = "You have to select More info option to explore the full detail of photography service!!";
            require __DIR__.'/../inc/message.inc.php'; ?>
          <?php endif; ?>
        </div><!-- inner div ending -->
      </div><!-- container div ending -->
     
    </section>
    
    <!-- Footer PHP File -->
    <?php 
    // adding footer file 
    require __DIR__.'/../inc/footer.inc.php'; 
    ?>   
  </body>
</html>