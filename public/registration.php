<?php 
  require __DIR__.'/../config.php';
  /**
   * Contact Page 
   * last_update: 2019-08-02
   * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
   * Site name : Khera Digital Studio and Color Lab
   */
  require __DIR__.'/../autoloaded.php';

  // list class dependecies in USE statments
  use App\Validator;

  // creating object
  $validator_obj = new Validator();

  $title = "Registration Form"; 
    
  // check for post method by server request
  if('POST' == $_SERVER['REQUEST_METHOD']) {
    //cheching for required fields
    $validator_obj->required('first_name');
    $validator_obj->required('last_name');
    $validator_obj->required('street');
    $validator_obj->required('city');
    $validator_obj->required('postal_code');
    $validator_obj->required('province');
    $validator_obj->required('country');
    $validator_obj->required('phone');
    $validator_obj->required('email_address');
    $validator_obj->required('password');
    $validator_obj->required('password_confirm');

    // check the form field validation
    $validator_obj->stringCheck('first_name');
    $validator_obj->stringCheck('last_name');
    $validator_obj->streetCheck('street');
    $validator_obj->stringCheck('city');
    $validator_obj->postalCodeCheck('postal_code');
    $validator_obj->stringCheck('province');
    $validator_obj->stringCheck('country');
    $validator_obj->phoneCheck('phone');
    $validator_obj->emailCheck('email_address');
    $validator_obj->passwordCheck('password');
    $validator_obj->passwordCheck('password_confirm');
    
    // comparing passwords
    $validator_obj->passwordsComapre('password','password_confirm');

    // check for email uniaueness so that two accounts doesnot have same email
    $validator_obj->emailUnique('email_address');

    // for displaying error
    $errors = $validator_obj->errors();

    // database entry start
    if(!count($errors)){
        
        // query for inserting data to the database
        $query = 'INSERT INTO 
                  customer
                  (first_name, last_name, street, city, postal_code, province, country, phone, email_address, password)
                  Values
                  (:first_name, :last_name, :street, :city, :postal_code, :province, :country, :phone, :email_address, :password)';
        //query preparation 
        $stmt = $dbh->prepare($query);
        
        // parameters array 
        $params = array(
            ':first_name' => $_POST['first_name'],
            ':last_name' => $_POST['last_name'],
            ':street' => $_POST['street'],
            ':city' => $_POST['city'],
            ':postal_code' => $_POST['postal_code'],
            ':province' => $_POST['province'],
            ':country' => $_POST['country'],
            ':phone' => $_POST['phone'],
            ':email_address' => $_POST['email_address'],
            ':password' => $_POST['password']
        );
        
        //query execution
        $stmt->execute($params);

        // adding last inset id 
        $customer_id = $dbh->lastInsertId();
        
        
        if($customer_id != 0){
          //adding the customer_id to session
          $_SESSION['customer_id'] = $customer_id;

          // setting login status to true;
          $_SESSION['log_in'] = true;

          // regenreation od customer_id
          session_regenerate_id(true);

          $_SESSION['message'] = 'You have successfully log in !';

          //PRG direction
          header('Location:profile.php');
          exit;
        }// end of if statement for customer id
        else{
          $_SESSION['message'] = 'There is some error. Please Try Again!';
        }//end of else statemnt
          
    }// end of empty errors
  }// end of post method
  



  // Adding man head file
  require __DIR__.'/../inc/head.inc.php';
?>
  
  <body>
   
    <!-- header PHP file -->
    <?php
    // adding header file
    require __DIR__.'/../inc/header.inc.php'; 
    ?>
    
    <div id="container"><!-- container div started -->
      <div id="inner"><!-- inner div started -->           
        <div><!-- div for form started -->
          <h1 style="padding-left: 30px;"><?=esc($title);?></h1>
          <p style="padding-left: 30px;">Please fill the form to chat with us:</p>
          <?php require __DIR__.'/../inc/message.inc.php'; ?>
          <form id="first_form"
                name="first_form"
                method="post"
                action="<?=esc_attr($_SERVER['PHP_SELF'])?>"
                autocomplete="on" 
                > <!-- starting of request form -->
            <fieldset>
              <legend>Personal Information</legend>
              <p>All the (<span style="color:#f00;">*</span>) fields are mandatory</p>
              <!-- cref protection -->
              <input type="hidden" name="csrf" value="<?=esc_attr($_SESSION['csrf'])?>" />
              <p>
                <label for="first_name" class="field">First Name:</label> 
                  <input type="text" 
                         name="first_name" 
                         id="first_name" 
                         maxlength="40"  
                         placeholder="Type Your First name"
                         value="<?=clean('first_name');?>"
                         /><!-- First name field -->
                  <?php if(!empty($errors['first_name'])) : ?>
                    <span class="error"><?=esc($errors['first_name'])?></span>
                  <?php endif; ?>
              </p>

              <p>
                <label for="last_name" class="field"> Last Name: </label> 
                  <input type="text" 
                         name="last_name" 
                         id="last_name" 
                         maxlength="40"
                         placeholder="Type Your Last name" 
                         value="<?=clean('last_name');?>"
                         /><!-- Last name field -->
                  <?php if(!empty($errors['last_name'])) : ?>
                    <span class="error"><?=esc($errors['last_name'])?></span>
                  <?php endif; ?>
              </p>
              
              <p>
                <label for="street" class="field">Street: </label>
                  <input type="text" 
                         name="street" 
                         id="street" 
                         placeholder="10 Jefferson Ave" 
                         value="<?=clean('street');?>"
                         /><!-- street input field -->
                  <?php if(!empty($errors['street'])) : ?>
                    <span class="error"><?=esc($errors['street'])?></span>
                  <?php endif; ?>
              </p>

              <p>
                <label for="city" class="field">City: </label>
                  <input type="text" 
                         name="city" 
                         id="city" 
                         placeholder="winnipeg" 
                         value="<?=clean('city');?>"
                         /><!-- city input field -->
                  <?php if(!empty($errors['city'])) : ?>
                    <span class="error"><?=esc($errors['city'])?></span>
                  <?php endif; ?>
              </p>

              <p>
                <label for="postal_code" class="field">Postal Code: </label>
                  <input type="text" 
                         name="postal_code" 
                         id="postal_code" 
                         placeholder="R2R 0D3" 
                         value="<?=clean('postal_code');?>"
                         /><!-- postal code input field -->
                  <?php if(!empty($errors['postal_code'])) : ?>
                    <span class="error"><?=esc($errors['postal_code'])?></span>
                  <?php endif; ?>
              </p>
                   
              <p>
                <label for="province" class="field">Province: </label>
                  <input type="text" 
                         name="province" 
                         id="province" 
                         placeholder="Manitoba" 
                         value="<?=clean('province');?>"
                         /><!-- province input field -->
                  <?php if(!empty($errors['province'])) : ?>
                    <span class="error"><?=esc($errors['province'])?></span>
                  <?php endif; ?>
              </p>

              <p>
                <label for="country" class="field">Country: </label>
                  <input type="text" 
                         name="country" 
                         id="country" 
                         placeholder="Canada" 
                         value="<?=clean('country');?>"
                         /><!-- country input field -->
                  <?php if(!empty($errors['country'])) : ?>
                    <span class="error"><?=esc($errors['country'])?></span>
                  <?php endif; ?>
              </p>

              <p>
                <label for="phone" class="field">Phone: </label>
                  <input type="text" 
                         name="phone" 
                         id="phone" 
                         placeholder="e.g:7856321452" 
                         value="<?=clean('phone');?>"
                         /><!-- phone input field -->
                  <?php if(!empty($errors['phone'])) : ?>
                    <span class="error"><?=esc($errors['phone'])?></span>
                  <?php endif; ?>
              </p>

              <p>
                <label for="email_address" class="field">Email: </label>
                  <input type="text" 
                         name="email_address" 
                         id="email_address" 
                         placeholder="e.g: soniaverma@gmail.com" 
                         value="<?=clean('email_address');?>"
                          /><!-- email input field -->
                  <?php if(!empty($errors['email_address'])) : ?>
                    <span class="error"><?=esc($errors['email_address'])?></span>
                  <?php endif; ?>
              </p>

              <p>
                <label for="password" class="field">Password: </label>
                  <input type="password" 
                         name="password" 
                         id="password" 
                         /><!-- password input field -->
                  <?php if(!empty($errors['password'])) : ?>
                    <span class="error"><?=esc($errors['password'])?></span>
                  <?php endif; ?>
              </p>

              <p>
                <label for="password_confirm" class="field">Password Confirm: </label>
                  <input type="password" 
                         name="password_confirm" 
                         id="password_confirm" 
                         /><!-- password confirm input field -->
                  <?php if(!empty($errors['password_confirm'])) : ?>
                    <span class="error"><?=esc($errors['password_confirm'])?></span>
                  <?php endif; ?>
              </p>
              
              <!-- action performed by submit and resrt button -->
              <button >Submit</button> 
              <input type="reset" value="Clear" /> 
                         
            </fieldset>

          </form>
        </div><!-- div for form ending -->
      </div><!-- inner div ending -->
    </div><!-- container div ending -->
    
    <!-- Footer PHP File -->
    <?php
    // adding footer file 
    require __DIR__.'/../inc/footer.inc.php'; 
    ?>  
  </body>
</html>