<?php 
  require __DIR__.'/../config.php';
  /**
   * Service Page 
   * last_update: 2019-08-02
   * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
   * Site name : Khera Digital Studio and Color Lab
   */  
  require __DIR__.'/../autoloaded.php';
  //give title to page
  $title = "Checkout";
  //using ordermodel
  use App\OrderModel;
  // instantiating the object
  $o = new OrderModel();
  //check for reuest for id
  if('GET' == $_SERVER['REQUEST_METHOD']){
    if(isset($_GET['target_id'])){
      $id = $_GET['target_id'];
      $data = $o->showOrder($id);
    }
  }
  
  // adding main head file
  require __DIR__.'/../inc/head.inc.php';


?>
  
  <body>
   
    <!-- header PHP file -->
    <?php
    // adding header file
    require __DIR__.'/../inc/header.inc.php'; 
    ?>
    
    <section>
      <div id="container"><!-- container div started -->
        <div id="inner"><!-- inner div started -->
          <a class="back" href="services.php" title="service page">Back to Services</a>
          
          <!-- cart information -->
          <?php if(empty($_SESSION['log_in']) || $_SESSION['log_in'] != true) : ?>
            <?php 
              $_SESSION['message'] = "You have to Book any services to see this page!!";
              require __DIR__.'/../inc/message.inc.php'; ?>
        	<?php else : ?>
          
            <!-- Services heading -->
            <h1><span class="underline">Thank you for your Order!</span></h1>
            <div class="table">
              <!-- table to display order confirmation -->
              <table>
                <tr>
                  <th>Company Info</th>
                  <th>Customer information</th>
                  <th>Order information</th>
                </tr>
                <tr>
                  <td>
                    <span>Khera Color Lab and Digital Studio</span><br />
                    <address>Nanak Nagri<br />
                    Moga, Punjab, 141003<br />
                    INDIA</address>
                    <span>98731-12585</span><br />
                    <span>92345-96325</span><br />
                    <span>Kheracolorlabmoga@gmail.com</span><br /><br />
                  </td>
                  <td>
                    <span>Name: </span><?=ucwords(esc($data['cust_name']))?><br />
                    <span>Street: </span><?=ucwords(esc($data['street']))?><br />
                    <span>City: </span><?=ucwords(esc($data['city']))?><br />
                    <span>Province: </span><?=ucwords(esc($data['province']))?><br />
                    <span>Postal Code: </span><?=ucwords(esc($data['postal_code']))?><br />
                    <span>Phone: </span><?=ucwords(esc($data['phone']))?><br />
                    <span>Email: </span><?=ucwords(esc($data['email_address']))?><br />
                  </td>
                  <td>
                    <span>Order no: </span><?=ucwords(esc($data['order_id']))?><br />
                    <span>Date: </span><?=ucwords(esc($data['order_date']))?><br />
                    <span>Price: </span>$<?=ucwords(esc($data['price']))?><br />
                    <span>GST: </span>$<?=ucwords(esc($data['gst']))?><br />
                    <span>PST: </span>$<?=ucwords(esc($data['pst']))?><br />
                    <span>Charged to Card: </span>$<?=ucwords(esc($data['total']))?><br />
                    <p>Please print the invoice for future records.</p>
                  </td>
                </tr> 
                <tr>
                  <th>Service Information</th>
                  <th  colspan="2">Price Information</th>
                </tr>
                <tr>
                  <td>
                    <span>Package: </span><?=ucwords(esc($data['package_type']))?><br />
                    <span>Session type: </span><?=ucwords(esc($data['session_type']))?><br />
                    <span>Photo Qaulity: </span><?=ucwords(esc($data['photo_quality']))?><br />
                    <span>Photo Size: </span><?=ucwords(esc($data['photo_size']))?><br />
                    <span>Photo Quantity: </span><?=ucwords(esc($data['no_of_photos']))?><br />
                    <span>Delivered by: </span><?=ucwords(esc($data['delivery_method']))?><br />
                    <span>Photographer name: </span><?=ucwords(esc($data['photographer_name']))?><br/>          
                  </td>
                  <td>
                    <span>Subtotal</span><br />
                    <span>GST</span><br />
                    <span>PST</span><br />
                    <span>Total</span><br />
                  </td>
                  <td>
                   $<?=ucwords(esc($data['price']))?><br />
                   $<?=ucwords(esc($data['gst']))?><br />
                   $<?=ucwords(esc($data['pst']))?><br />
                   $<?=ucwords(esc($data['total']))?><br /> 
                  </td>
                </tr>
                <tr >
                  <th style="text-align: center;" colspan="3">Have a Nice Day!</th>
                </tr>
              </table>
            </div>
          <?php endif; ?>

        </div><!-- inner div ending -->
      </div><!-- container div ending -->
     
    </section>
    
    <!-- Footer PHP File -->
    <?php 
    // adding footer file 
    require __DIR__.'/../inc/footer.inc.php'; 
    ?>   
  </body>
</html>
