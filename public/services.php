<?php 
  require __DIR__.'/../config.php';
  /**
   * Service Page 
   * last_update: 2019-08-02
   * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
   * Site name : Khera Digital Studio and Color Lab
   */
  
  require __DIR__.'/../autoloaded.php';
  $title = "Services";

  // using services from models
  use App\ServiceModel;
  
  // instantiating the object
  $s = new ServiceModel;
  $services = $s->all();
  //$services = $s->filterData();

  // using search option to filter the list
  
  if(isset($_POST['search'])){
    $filter = $_POST['search'];
    // if filter exists then 
    if($filter){
      $query = "SELECT * FROM services where package_type LIke '%$filter%'";
      $stmt = $dbh->prepare($query);
      $stmt->execute();
      $services = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      if(empty($result)){
        $_SESSION['message'] = "Your result not found!!";
      }// end of result if statement
    }// end of filter if statement
    else{
      $_SESSION['message'] = "Nothing to search!!";
    }// end of else statemnt 
  }//end of isset if statement 

  if(isset($_GET['delivery_method'])){
    $method = $_GET['delivery_method'];
    $services = $s->filterData($method);
  }

  // adding main head file
  require __DIR__.'/../inc/head.inc.php';


?>
  
  <body>
   
    <!-- header PHP file -->
    <?php
    // adding header file
    require __DIR__.'/../inc/header.inc.php'; 
    ?>
    
    <section>
      <div id="container"><!-- container div started -->
        <div id="inner"><!-- inner div started -->
          <!-- Services heading -->
          <h1><span class="underline">Services</span></h1>
          <div id="secondary">
            <form method="post" action="<?=esc_attr($_SERVER['PHP_SELF'])?>">
              <!-- cref protection -->
              <input type="hidden" name="csrf" value="<?=esc_attr($_SESSION['csrf']);?>" />
              <input type="text" name="search" id="search" placeholder="search"/>
              <button type="submit" class="search-btn"> Search</button>
            </form>
            <br />
            <h2>Categories</h2>
            <ul class="category">
              <li><strong>Deivery Method</strong>
                <!-- loop for displaying data -->
                <ul>
                  <li><a href="services.php?delivery_method='Album'">Album</a></li>
                  <li><a href="services.php?delivery_method='Pen Drive'">Pen drive</a></li>
                  <li><a href="services.php?delivery_method='Google Drive'">Google Drive</a></li>
                  <li><a href="services.php?delivery_method='Hard Disk'">Hard Disk</a></li>
                  <li><a href="services.php?delivery_method='Prints'">Prints</a></li>
                  <li><a href="services.php?delivery_method='CDs'">CDs</a></li>
                </ul>
              </li>
            </ul>
          </div><!-- Secondary div end -->
         
          <div class="main">
            <!-- loop for displaying data -->
            <?php foreach($services as $service): ?>
              <div class="item">
              <div class="img">
                <img src="Images/<?=esc_attr($service['image'])?>" alt="pic_s3"/>
              </div>
              <div class="details">
                <p><strong><?=esc($service['package_type'])?></strong>
                  : <?=esc($service['session_type'])?> session<br /><br />
                  <span>Photo quality: </span><?=esc($service['photo_quality'])?><br />
                  <span>Photo size: </span><?=esc($service['photo_size'])?><br />
                  <span>Photo quantity: </span><?=esc($service['no_of_photos'])?><br />
                  <span>Delivery method: </span><?=esc($service['delivery_method'])?><br />
                  <span>Price: </span>$<?=esc($service['price'])?></p>
                </div>
                <div class="more">
                  <a class="search-btn" 
                          href="service_detail.php?id=<?=esc_attr($service['services_id'])?>">
                    More info
                  </a>
                </div>
              </div>
            <?php endforeach; ?>
          </div><!-- main div ending -->
        </div><!-- inner div ending -->
      </div><!-- container div ending -->
     
    </section>
    
    <!-- Footer PHP File -->
    <?php 
    // adding footer file 
    require __DIR__.'/../inc/footer.inc.php'; 
    ?>   
  </body>
</html>