<?php 
  
  require __DIR__.'/../config.php';
  /**
   * login Page 
   * last_update: 2019-08-02
   * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
   * Site name : Khera Digital Studio and Color Lab
   */
  require __DIR__.'/../autoloaded.php';

  // list class dependecies in USE statments
  use App\Validator;

  // creating object
  $validator_obj = new Validator();

  $title = "Login"; 

  if(!empty($_GET['logout'])) {
    //unsetong the session
    unset($_SESSION['log_in']);
    unset($_SESSION['admin']);
    //regenrate the session id
    session_regenerate_id();
    //unseting the session cart
    unset($_SESSION['cart_item']);
    unset($_SESSION['target']);
    unset($_SESSION['customer_id']);
    //setting the flash messgae
    $_SESSION['message'] = 'You have been successfully logged out.';

    //PRG Direction to l ogin php
    header('Location: login.php');
    die;
  }


  // check for post method
  if('POST' == $_SERVER['REQUEST_METHOD']){
    $validator_obj->required('email_address');
    $validator_obj->required('password');

    // for displaying error
    $errors = $validator_obj->errors();

    // database entry start
    if(!count($errors)){

      //query preparation
      $query1 = 'SELECT * FROM customer WHERE email_address = :email_address';
      //prepare the databse
      $stmt1 = $dbh->prepare($query1);
      //array for paramerts
      $params1 = array(':email_address' => $_POST['email_address']);
      // query execution
      $stmt1->execute($params1);

      // fetching all the query 
      $result_data = $stmt1->fetch();
     
      
      if(password_verify($_POST['password'], $result_data['password'])) {
        // set $_SESSION['logged_in'] to true
        $_SESSION['log_in'] = true;
        $_SESSION['customer_id'] = esc($result_data['customer_id']);
        $_SESSION['message'] = 'Congrats, You have logged in! Welcome Back, '. ucwords(esc($result_data['first_name']));
        $admin = esc($result_data['admin']);
        // regenerate the session id
        session_regenerate_id(true);
        // if conditon to check for target session
        // if it true then redirected to targetpage
        // otherwise profile page
        if($admin == 1){
          $_SESSION['admin'] = true;
          header('Location:/admin/index.php');
          die;
        }
        // check session target
        if(!empty($_SESSION['target'])){
          $target = esc($_SESSION['target']);
          // Redirect to profile.php
          header("Location: $target");
          // die
          die;
        }else{
          header('Location:profile.php');
          die;
        }

      } 
      else {
        // unset $_SESSION['logged_in']
        unset($_SESSION['log_in']);
        // $errors[] = login credentials do not match
        $_SESSION['message'] = 'Login credentials do not match';
      }

      if(empty($result_data)){
            $_SESSION['message'] = 'No such user here';
        }

    }// end of count if statement

    
  }// end of post if statement


  // adding main head file
  require __DIR__.'/../inc/head.inc.php'; 
?>
  
  <body>
   
    <!-- header PHP file -->
    <?php 

    // header file included
    require __DIR__.'/../inc/header.inc.php'; 
    ?>
    
    <section>
      <div id="container"><!-- container div id starting -->
        <div id="inner"><!-- inner div starting -->
          <h1 style="padding-left: 30px;">Login Form:</h1>
          <p style="padding-left: 30px;">Please fill the form to chat with us:</p>
          <?php require __DIR__.'/../inc/message.inc.php'; ?>
          <form id="first_form"
                name="first_form"
                method="post"
                action="<?=esc_attr($_SERVER['PHP_SELF'])?>"
                autocomplete="on" 
                > <!-- starting of request form -->
            <fieldset>
              <legend>Login Information</legend>
              <p>All the (<span style="color:#f00;">*</span>) fields are mandatory</p>
              <!-- cref protection -->
              <input type="hidden" name="csrf" value="<?=esc_attr($_SESSION['csrf'])?>" />
              <!-- email address -->              
              <p>
                <label for="email_address" class="field">Email: </label>
                  <input type="text" 
                         name="email_address" 
                         id="email_address" 
                         placeholder="e.g: soniaverma@gmail.com" 
                         value="<?=clean('email_address');?>" 
                          /><!-- email input field -->
                  <?php if(!empty($errors['email_address'])) : ?>
                    <span class="error"><?=esc($errors['email_address'])?></span>
                  <?php endif; ?>
              </p>
              <!-- password field -->
              <p>
                <label for="password" class="field">Password: </label>
                  <input type="password" 
                         name="password" 
                         id="password" 
                         /><!-- password input field -->
                  <?php if(!empty($errors['password'])) : ?>
                    <span class="error"><?=esc($errors['password'])?></span>
                  <?php endif; ?>
              </p>

              <!-- action performed by submit and resrt button -->
              <button>Submit</button> 
                                      
            </fieldset>

          </form>
          <p style="padding-left: 30px;"><span style="color: #ff0;">*( If You do not have login id and password then REGISTER yourself first. )</span></p>
        </div><!-- inner div ending -->
      </div><!-- container div ending -->
    </section>
    
    <!-- Footer PHP File -->
    <?php 
    // adding footer file 
    require __DIR__.'/../inc/footer.inc.php'; 
    ?>  
  </body>
</html> 