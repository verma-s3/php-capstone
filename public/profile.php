<?php 
  // adding config file.
  require __DIR__.'/../config.php';
  /**
   * Contact Page 
   * last_update: 2019-08-02
   * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
   * Site name : Khera Digital Studio and Color Lab
   */
  $title = "Profile";
 
  // check for empty request
  if(empty($_SESSION['log_in']) || $_SESSION['log_in'] != true){

    // setting meassage
    $_SESSION['message'] = 'You have to login to see you profile';
    header('Location: login.php');
    die();
  }

  //redirecting the query from registration form.
  if('GET' == $_SERVER['REQUEST_METHOD']){

    // query for seleting data
    $query = 'select * from customer where customer_id = :customer_id';
    // preparing database
    $stmt = $dbh->prepare($query);
      
    $params = array(':customer_id'=> $_SESSION['customer_id']);

    //execute database
    $stmt->execute($params);
    
    // fetching queries
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
  }

  // Adding man head file
  require __DIR__.'/../inc/head.inc.php'; 
  
?>
  
  <body>
   
    <!-- header PHP file -->
    <?php
    // adding header file
    require __DIR__.'/../inc/header.inc.php'; 
    ?>
    
    <div id="container"><!-- container div started -->
      <div id="inner"><!-- inner div started -->
        <?php require __DIR__.'/../inc/message.inc.php'; ?>
        <h1><?="Thank You for Registering, <span style='color:#ff3;'>".ucwords(esc($result['first_name']))?></h1>
          <!-- table to getting form data -->
          <table>
            <tr>
              <th>First name</th>
              <td><?=esc($result['first_name'])?></td>
            </tr>
            <tr>
              <th>Last name</th>
              <td><?=esc($result['last_name'])?></td>
            </tr>
            <tr>
              <th>Street</th>
              <td><?=esc($result['street'])?></td>
            </tr>
            <tr>
              <th>City</th>
              <td><?=esc($result['city'])?></td>
            </tr>
            <tr>
              <th>Postal Code</th>
              <td><?=esc($result['postal_code'])?></td>
            </tr>
            <tr>
              <th>Province</th>
              <td><?=esc($result['province'])?></td>
            </tr>
            <tr>
              <th>Country</th>
              <td><?=esc($result['country'])?></td>
            </tr>
            <tr>
              <th>Phone</th>
              <td><?=esc($result['phone'])?></td>
            </tr>
            <tr>
              <th>Email</th>
              <td><?=esc($result['email_address'])?></td>
            </tr>
            
          </table><!-- end of table -->
        <p style="padding: 20px 120px;">Khera Digital Studio and Color Lab welcomes you being a member of our company. we are pleasure to having customer like you.</p>
      </div><!-- inner div ending -->
    </div><!-- container div ending -->
    
    <!-- Footer PHP File -->
    <?php
    // adding footer file 
    require __DIR__.'/../inc/footer.inc.php'; 
    ?>  
  </body>
</html>