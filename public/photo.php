<?php 
  require __DIR__.'/../config.php';
  /**
   * Photo Page 
   * last_update: 2019-08-02
   * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
   * Site name : Khera Digital Studio and Color Lab
   */
  $title = "Photo";
  // adding main head file
  require __DIR__.'/../inc/head.inc.php';  
?>
  
  <body>
    
    <!-- header PHP file -->
    <?php
    // adding header file
    require __DIR__.'/../inc/header.inc.php'; 
    ?>
    
    <section>
      <div id="container"><!-- container div started -->
        <div id="inner"><!-- inner div started -->
          <h1><span class="underline">Photography</span></h1>
          <p>
            Khera Color Lab and Digital Studio has expertise team for the creative photography, do not metters
            what the event is. DSLR photos are quite popular nowadays. Blur background with focus on a person makes 
            photos more interesting and unique. Prople love to have these types of memorable momemts of their life.
            Our motive is not only to provide quality work to our customers but also to keep their expectations and
            requirements at first.
          </p>
          <div id="photograhy"><!-- photography starting -->
            <!-- gallery imagaes inside div starting -->
            <div class="gallery_pic">
              <img src="Images/g1.jpg" alt="photo" />
              <div class="overlay">
                <p class="text">Ankita and Lucky</p>
              </div>
            </div>
            <div class="gallery_pic">
              <img src="Images/g2.jpg" alt="photo" />
              <div class="overlay">
                <p class="text">Purva and Rahul</p>
              </div>
            </div>
            <div class="gallery_pic">
              <img src="Images/g3.jpg" alt="photo" />
              <div class="overlay">
                <p class="text">Nisha and Vikas</p>
              </div>
            </div>
            <div class="gallery_pic">
              <img src="Images/g4.jpg" alt="photo" />
              <div class="overlay">
                <p class="text">Pooja and Akash</p>
              </div>
            </div>
            <div class="gallery_pic">
              <img src="Images/g5.jpg" alt="photo" />
              <div class="overlay">
                <p class="text">Aman and Deepak</p>
              </div>
            </div>
            <div class="gallery_pic">
              <img src="Images/g6.jpg" alt="photo" />
              <div class="overlay">
                <p class="text">Nidhi and Sandeep</p>
              </div>
            </div>
            <div class="gallery_pic">
              <img src="Images/g7.jpg" alt="photo" />
              <div class="overlay">
                <p class="text">Nitika and Rajan</p>
              </div>
            </div>
            <div class="gallery_pic">
              <img src="Images/g8.jpg" alt="photo" />
              <div class="overlay">
                <p class="text">Raman and Sachin</p>
              </div>
            </div>
            <!-- gallery imagaes inside div ending -->
          </div><!-- photographer div ending -->
        </div><!-- inner div ending -->
      </div><!-- container div ending -->
    </section>
   
    <!-- Footer PHP File -->
    <?php 
    // adding footer file 
    require __DIR__.'/../inc/footer.inc.php'; 
    ?>   
  </body>
</html>