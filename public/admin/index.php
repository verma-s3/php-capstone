<?php

  require __DIR__.'/../../config.php';
    /**
     * Admin panel - index page
     * last_update: 2019-08-02
     * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
     * Site name : Khera Digital Studio and Color Lab
     */


  require __DIR__.'/../../autoloaded.php';
  
  // using services from models
  use App\ServiceModel;
  use App\CustomerModel;
  use App\OrderModel;
  
  //to deplay serial number
  $number = 0;
  // instantiating the object
  $s = new ServiceModel;
  $c = new CustomerModel;
  $o = new OrderModel;
  // calling count function
  $service_count = $s->count();
  $order_count = $o->count();
  $customer_count = $c->count();
  // calling max function
  $service_max = $s->max();
  $order_max = $o->max();
  //calling min function
  $service_min = $s->min();
  $order_min = $o->min();
  // calling average function
  $service_avg = $s->avg();
  $order_avg = $o->avg();
 

  require __DIR__.'/inc/head.inc.php';
?>
	<!-- cart information -->
  <?php if(empty($_SESSION['admin']) || $_SESSION['admin'] != true) : ?>
    <header style="min-height: 700px;padding-left: 60px">
      <?php 
      $_SESSION['message'] = "You have to login to see the Admin Panel!!";
      header('Location:/../login.php');
      //require __DIR__.'/../../inc/message.inc.php'; ?>
      <h1><?=esc($_SESSION['message'])?></h1>
    </header>
  <?php else : ?>
  <header>
    <h1>Dashboard</h1>
  </header> 
  <table class="table"  style="color: #fff;">
    <tr style="color: #34495e; background: #fff;">
      <th colspan="2" style="text-align: center;">Overview</th>
      <th colspan="2" style="text-align: center;">Services</th>
      <th colspan="2" style="text-align: center;">Orders</th>
    </tr>
    <tr>
      <td>Total Services: </td>
      <td class="td"><?=esc($service_count['count'])?></td>
      <td>Maximum Price: </td>
      <td class="td">$<?=esc($service_max['max'])?></td>
      <td>Maximum Order: </td>
      <td>$<?=esc($order_max['max'])?></td>
    </tr>
     <tr>
      <td>Total Customers: </td>
      <td class="td"><?=esc($customer_count['count'])?></td>
      <td>Minimum Price: </td>
      <td class="td">$<?=esc($service_min['min'])?></td>
      <td>Minimum Order: </td>
      <td>$<?=esc($order_min['min'])?></td>
    </tr> 
     <tr>
      <td>Total Order: </td>
      <td class="td"><?=esc($order_count['count'])?></td>
      <td>Average Price: </td>
      <td class="td">$<?=esc($service_avg['avg'])?></td>
      <td>Average Order: </td>
      <td>$<?=esc($order_avg['avg'])?></td>
    </tr>
  </table>
  <table class="table"  style="color: #fff;">
    <tr style="color: #34495e; background: #fff;">
      <th style="text-align: center;" rowspan="2">Report of Recent Log Entities</th>
    </tr>
    <tr style="color: #fff; background: rgba(0,0,0,0.5);">
      <th style="text-align: left;" colspan="2">Recent Log ( Newest First )</th> 
    </tr>
     
    
      <?php foreach ($asc as $ascending): ?>
        <?php $number = $number+1; ?>
        <tr>
          <td><?=esc($number);?></td>
        <td class="odd"><?=esc($ascending['event']); ?></td>
        </tr>
      <?php endforeach; ?>
    
  
  </table>
   
<?php endif; ?>
<!--  including footer file -->
<?php require __DIR__.'/inc/footer.inc.php'; ?>