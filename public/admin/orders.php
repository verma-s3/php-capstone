<?php

  require __DIR__.'/../../config.php';
    /**
     * Admin panel - index page
     * last_update: 2019-08-02
     * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
     * Site name : Khera Digital Studio and Color Lab
     */


  require __DIR__.'/../../autoloaded.php';
  
  // using services from models
  use App\OrderModel;
  
  // instantiating the object
  $o = new OrderModel;
  $orders = $o->allOrder();
    
  require __DIR__.'/inc/head.inc.php';
?>
	<!-- cart information -->
  <?php if(empty($_SESSION['admin']) || $_SESSION['admin'] != true) : ?>
    <header style="min-height: 700px;padding-left: 60px">
      <?php 
      $_SESSION['message'] = "You have to login to see the records!!";
      header('Location:/../login.php');
      //require __DIR__.'/../../inc/message.inc.php'; ?>
      <h1><?=esc($_SESSION['message'])?></h1>
    </header>
  <?php else : ?>
  <header>
    <h1>Order Table</h1>
  </header> 
  <form method="post" action="<?=esc_attr($_SERVER['PHP_SELF'])?>">
    <!-- cref protection -->
    <input type="hidden" name="csrf" value="<?=esc_attr($_SESSION['csrf']);?>" />
    <a type="submit" href="index.php" class="search-btn"> Back</a>
  </form>
  
  <table class="table"  style="color: #fff;">
    <tr>
      <th>Order no.</th>
      <th>Order date</th>
      <th>Package_type</th>
      <th>Customer name</th>
      <th>Customer Email</th>
      <th>Customer Phone no.</th>
      <th>Price</th>
      <th>GST</th>
      <th>PST</th>
      <th>Total Price</th>
    </tr>
    <!-- loop for displaying data -->
    <?php foreach($orders as $order): ?>
      <tr>
        <td><?=esc($order['order_id'])?></td>
        <td><?=esc($order['order_date'])?></td>
        <td><?=esc($order['package_type'])?></td>
        <td><?=esc($order['cust_name'])?></td>
        <td><?=esc($order['email_address'])?></td>
        <td><?=esc($order['cust_phone'])?></td> 
        <td>$<?=esc($order['price'])?></td> 
        <td>$<?=esc($order['gst'])?></td>
        <td>$<?=esc($order['pst'])?></td>
        <td>$<?=esc($order['total'])?></td>
      </tr>
      <?php endforeach; ?>
  </table>  
<?php endif; ?>
<!--  including footer file -->
<?php require __DIR__.'/inc/footer.inc.php'; ?>