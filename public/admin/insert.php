<?php

  require __DIR__.'/../../config.php';
    /**
     * Contact Page 
     * last_update: 2019-08-02
     * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
     * Site name : Khera Digital Studio and Color Lab
     */


  require __DIR__.'/../../autoloaded.php';

  // using services from models
  use App\ServiceModel;
  use App\Validator;
  
  // instantiating the object
  $e = new ServiceModel;
  $v = new Validator();
  
  // if conditino check for post request method
  if('POST' == $_SERVER['REQUEST_METHOD']){
    
    $id = $_REQUEST['services_id'];
    $services = $e->one($id);
    // checking for required fields
    $v->required('package_type');
    $v->required('session_type');
    $v->required('photo_quality');
    $v->required('photo_size');
    $v->required('no_of_photos');
    $v->required('delivery_method');
    $v->required('price');
    $v->required('image');
    $v->required('photographer_name');
    
    // check for validation of fiels
    $v->stringCheck('package_type');
    $v->sessionQuality('session_type');
    $v->sessionQuality('photo_quality');
    $v->size('photo_size');
    $v->quantity('no_of_photos');
    $v->stringCheck('delivery_method');
    $v->price('price');
    $v->stringCheck('photographer_name');
    
    // lenth validation
    $v->length('package_type');
    $v->length('photographer_name');
    
    // for displaying error
    $errors = $v->errors();
    // refernces: steve's code
    if(!empty($_FILES['image']['tmp_name'])) {
        var_dump($_FILES);
        $target_path = __DIR__ . '/images/';
        $file_name = $_FILES['image']['name'];
        // To avoid overwriting existing files, you can do one of two things:
        // 1. check to see if the file exists, and if so, output an error
        // message, and do not move the uploaded file
        // 2. Just add a unique string the new file name so it does not 
        // overrwrite existing images:
        $file_name = uniqid() . '-' . $_FILES['image']['name'];
        $source = $_FILES['image']['tmp_name'];
        $destination = $target_path . $file_name;
        // is this file actually an image?
        if(getimagesize($source)) {
            // if so, move it from temp location to a final location
            move_uploaded_file($source, $destination);
            echo "<img src='images/$file_name' />";
        } else {
            die('The file you uploaded was not an image!');
        }        
    } // end if $_FILES

    // check for errro counting
    if(!count($errors)){
      $rows = $e->addData($_POST);
      var_dump($rows);
      if($rows){
      // getting the id 
      //$id = $_REQUEST['id'];    
      // redirection to othe rpage
      header('Location:services.php');
      $_SESSION['message'] = "Data added sucessfully!!";  
      die;
      }// endif of $rows==0
    }// end of count errors
  }// end of post if 
 
  require __DIR__.'/inc/head.inc.php';
?>
  <!-- cart information -->
  <?php if(empty($_SESSION['admin']) || $_SESSION['admin'] != true) : ?>
    <header style="min-height: 700px;padding-left: 60px">
      <?php 
      $_SESSION['message'] = "You have to login to see the records!!";
      header('Location:/../login.php');
      //require __DIR__.'/../../inc/message.inc.php'; ?>
      <h1><?=esc($_SESSION['message'])?></h1>
    </header>
  <?php else : ?>
	<!-- display of order -->
  <header>
    <h1>Add Service</h1>	
  </header>
  <!--back button to go to previous page -->
  <form method="post" action="<?=esc_attr($_SERVER['PHP_SELF'])?>">
    <!-- cref protection -->
    <input type="hidden" name="csrf" value="<?=esc_attr($_SESSION['csrf'])?>" />
    <a type="submit" href="services.php" class="search-btn"> Back</a>
  </form>
  <!-- form to diaply the data that for editing -->
  <div class="form">
    <form action="insert.php" method="post"  enctype="multipart/form-data'" >
      <!-- cref protection -->
      <input type="hidden" name="csrf" value="<?=esc_attr($_SESSION['csrf'])?>" />
      <p>
        <label for="services_id"></label>
        <input type="hidden" name="services_id" id="services_id" value="<?=clean('services_id')?>" />
      </p><br /> 
      <!-- Package type -->
      <p>
        <label for="package_type" class="field">Package</label>
        <input type="text" name="package_type" id="package_type" value="<?=clean('package_type')?>" />
        <?php if(!empty($errors['package_type'])) : ?>
          <span class="error"><?=esc($errors['package_type'])?></span>
        <?php endif; ?>
      </p>
      <!-- session type -->
      <p> 
        <label for="session_type" class="field">Session time</label>
        <input type="text" name="session_type" id="session_type" value="<?=clean('session_type')?>" />
        <?php if(!empty($errors['session_type'])) : ?>
          <span class="error"><?=esc($errors['session_type'])?></span>
        <?php endif; ?>
      </p>
      <!-- photo quality -->
      <p>
        <label for="photo_quality" class="field">Quality</label>
        <input type="text" name="photo_quality" id="photo_quality" value="<?=clean('photo_quality')?>" />
        <?php if(!empty($errors['photo_quality'])) : ?>
          <span class="error"><?=esc($errors['photo_quality'])?></span>
        <?php endif; ?>
      </p>
      <!-- photo size --> 
      <p>
        <label for="photo_size" class="field">Size</label>
        <input type="text" name="photo_size" id="photo_size" value="<?=clean('photo_size')?>" />
        <?php if(!empty($errors['photo_size'])) : ?>
          <span class="error"><?=esc($errors['photo_size'])?></span>
        <?php endif; ?>
      </p>
      <!-- no of photos --> 
      <p>
        <label for="no_of_photos" class="field">Quantity</label>
        <input type="text" name="no_of_photos" id="no_of_photos" value="<?=clean('no_of_photos')?>" />
        <?php if(!empty($errors['no_of_photos'])) : ?>
          <span class="error"><?=esc($errors['no_of_photos'])?></span>
        <?php endif; ?>
      </p>
      <!-- delivery method -->
      <p>
        <label for="delivery_method" class="field">Delivery type</label>
        <input type="text" name="delivery_method" id="delivery_method" value="<?=clean('delivery_method')?>" />
        <?php if(!empty($errors['delivery_method'])) : ?>
          <span class="error"><?=esc($errors['delivery_method'])?></span>
        <?php endif; ?>
      </p>
      <!-- price -->
      <p>
        <label for="price" class="field">Price</label>
        <input type="text" name="price" id="price" value="<?=clean('price')?>" />
        <?php if(!empty($errors['price'])) : ?>
          <span class="error"><?=esc($errors['price'])?></span>
        <?php endif; ?>
      </p>
      <!-- image -->
      <p>
        <label for="image" class="field">Image</label>
        <input type="file" name="image" id="image" value="<?=clean('image')?>" />
        <?php if(!empty($errors['image'])) : ?>
          <span class="error"><?=esc($errors['image'])?></span>
        <?php endif; ?>
      </p>
      <!-- photographer name-->  
      <p>
        <label for="photographer_name" class="field">Photographer</label>
        <input type="text" name="photographer_name" id="photographer_name" value="<?=clean('photographer_name')?>" />
        <?php if(!empty($errors['photographer_name'])) : ?>
          <span class="error"><?=esc($errors['photographer_name'])?></span>
        <?php endif; ?>
      </p>
      <!-- submit button -->
      <button type="submit" value="update">Click to Add Service</button>  
    </form>
  </div>
<?php endif; ?>
<?php require __DIR__.'/inc/footer.inc.php'; ?>

