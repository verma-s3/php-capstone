<?php

  require __DIR__.'/../../config.php';
    /**
     * Admin panel - index page
     * last_update: 2019-08-02
     * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
     * Site name : Khera Digital Studio and Color Lab
     */


  require __DIR__.'/../../autoloaded.php';
  
  // using services from models
  use App\CustomerModel;
  
  // instantiating the object
  $c = new CustomerModel;
  $customers = $c->all();
  // including head file.  
  require __DIR__.'/inc/head.inc.php';
?>
	<!-- cart information -->
  <?php if(empty($_SESSION['admin']) || $_SESSION['admin'] != true) : ?>
    <header style="min-height: 700px;padding-left: 60px">
      <?php 
      $_SESSION['message'] = "You have to login to see the records!!";
      header('Location:/../login.php');
      //require __DIR__.'/../../inc/message.inc.php'; ?>
      <h1><?=esc($_SESSION['message'])?></h1>
    </header>
  <?php else : ?>
  <header>
    <h1>Customers Table</h1>
  </header> 
  <form method="post" action="<?=esc_attr($_SERVER['PHP_SELF'])?>">
    <!-- cref protection -->
    <input type="hidden" name="csrf" value="<?=esc_attr($_SESSION['csrf']);?>" />
    <a type="submit" href="index.php" class="search-btn"> Back</a>
  </form>
  <!-- services information -->
  <div><?php require __DIR__.'/../../inc/message.inc.php'; ?></div>
  <table class="table"  style="color: #fff;">
    <tr>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Street</th>
      <th>City</th>
      <th>Province</th>      
      <th>Country</th>
      <th>Postal Code</th>
      <th>Phone</th>
      <th>Email</th>
    </tr>
    <!-- loop for displaying data -->
    <?php foreach($customers as $customer): ?>
      <tr>
        <td><?=esc($customer['first_name'])?></td>
        <td><?=esc($customer['last_name'])?></td>
        <td><?=esc($customer['street'])?></td>
        <td><?=esc($customer['city'])?></td>
        <td><?=esc($customer['province'])?></td>
        <td><?=esc($customer['country'])?></td> 
        <td><?=esc($customer['postal_code'])?></td> 
        <td><?=esc($customer['phone'])?></td>
        <td><?=esc($customer['email_address'])?></td>
      <?php endforeach; ?>
  </table>  
<?php endif; ?>
<!--  including footer file -->
<?php require __DIR__.'/inc/footer.inc.php'; ?>