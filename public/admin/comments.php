<?php

  require __DIR__.'/../../config.php';
    /**
     * Admin panel - index page
     * last_update: 2019-08-02
     * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
     * Site name : Khera Digital Studio and Color Lab
     */


  require __DIR__.'/../../autoloaded.php';
  
  // using services from models
  use App\CommentsModel;
  
  // instantiating the object
  $c = new CommentsModel;
  $comments = $c->all();
   
  //including the head file
  require __DIR__.'/inc/head.inc.php';
?>
	<!-- cart information -->
  <?php if(empty($_SESSION['admin']) || $_SESSION['admin'] != true) : ?>
    <header style="min-height: 700px;padding-left: 60px">
      <?php 
      $_SESSION['message'] = "You have to login to see the records!!";
      header('Location:/../login.php');
      //require __DIR__.'/../../inc/message.inc.php'; ?>
      <h1><?=esc($_SESSION['message'])?></h1>
    </header>
  <?php else : ?>
  <header>
    <h1>Comments Table</h1>
  </header> 
  <form method="post" action="<?=esc_attr($_SERVER['PHP_SELF'])?>">
    <!-- cref protection -->
    <input type="hidden" name="csrf" value="<?=esc_attr($_SESSION['csrf']);?>" />
    <a type="submit" href="index.php" class="search-btn"> Back</a>
  </form>
  <!-- comments information -->
  <table class="table"  style="color: #fff;">
    <tr>
      <th>Name</th>
      <th>Email Address</th>
      <th>Comments</th>
      <th>Date</th>
    </tr>
    <!-- loop for displaying data -->
    <?php foreach($comments as $comment): ?>
      <tr>
        <td><?=esc($comment['first_name'])?></td>
        <td><?=esc($comment['email_address'])?></td>
        <td><?=esc($comment['comments'])?></td>
        <td><?=esc($comment['created_at'])?></td>
      </tr>
      <?php endforeach; ?>
  </table>  
<?php endif; ?>
<!--  including footer file -->
<?php require __DIR__.'/inc/footer.inc.php'; ?>