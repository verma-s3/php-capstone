<?php

  require __DIR__.'/../../config.php';
    /**
     * Admin panel - index page
     * last_update: 2019-08-02
     * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
     * Site name : Khera Digital Studio and Color Lab
     */


  require __DIR__.'/../../autoloaded.php';
  
  // using services from models
  use App\ServiceModel;
  
  // instantiating the object
  $s = new ServiceModel;
  $services = $s->all();
  
  // check for search request
  if(isset($_POST['search'])){
    $filter = $_POST['search'];
    // if filter exists then 
    if($filter){
      $query = "SELECT * FROM services where package_type LIke '%$filter%'";
      $stmt = $dbh->prepare($query);
      $stmt->execute();
      $services = $stmt->fetchAll(\PDO::FETCH_ASSOC);
      if(empty($services)){
        $_SESSION['message'] = "Your result not found!!";
      }// end of result if statement
    }// end of filter if statement
    else{
      $_SESSION['message'] = "Nothing to search!!";
    }// end of else statemnt 
  }//end of isset if statement 

  // check for get method
  if('GET' == $_SERVER['REQUEST_METHOD']){
    if(isset($_GET['target_id'])){
      $id = $_GET['target_id'];
      //$service = $s->one($id);
      //using try and catch block to delete data
    try{
      //start the trantion
        $dbh->beginTransaction();
        $s->insertData($id);
        
        // making query
        $query1 = "DELETE from services where services_id = :id "; 
        // preparing query
        $stmt = $dbh->prepare($query1);
        $params = array(':id' => $id);
        // exceuting quesy
        $stmt->execute($params);
        //commint the query
        $dbh->commit();
        header('Location:services.php');
    }
    catch(EXCEPTION $e){
      $dbh->rollback();
      echo $e->getMessage();
      die();
    }
    }//end if isset 
  }//endif for GET server request
  
  require __DIR__.'/inc/head.inc.php';
?>
	<!-- cart information -->
  <?php if(empty($_SESSION['admin']) || $_SESSION['admin'] != true) : ?>
    <header style="min-height: 700px;padding-left: 60px">
      <?php 
      $_SESSION['message'] = "You have to login to see the records!!";
      header('Location:/../login.php');
      //require __DIR__.'/../../inc/message.inc.php'; ?>
      <h1><?=esc($_SESSION['message'])?></h1>
    </header>
  <?php else : ?>
  <header>
    <h1>Services Table</h1>
  </header> 
  <form method="post" action="<?=esc_attr($_SERVER['PHP_SELF'])?>" >
    <!-- cref protection -->
    <input type="hidden" name="csrf" value="<?=esc_attr($_SESSION['csrf']);?>" />
    <button type="submit" class="search-btn pull-right"> Search</button>
    <input type="text" name="search" id="search" placeholder="search" class ="pull-right"/>
    <a type="submit" href="index.php" class="search-btn"> Back</a>
    <a href="insert.php" class="search-btn "> 
      Insert new Service
    </a>
  </form>
  <!-- services information -->
  <div><?php require __DIR__.'/../../inc/message.inc.php'; ?></div>
  <table class="table"  style="color: #fff;">
    <tr>
      <th>Package</th>
      <th>Session time</th>
      <th>Quality</th>
      <th>Size</th>
      <th>Quantity</th>
      <th>Delievery by</th>
      <th>Price</th>
      <th>Image</th>
      <th>Photograher</th>
      <th style="text-align: center">Action</th>
    </tr>
    <!-- loop for displaying data -->
    <?php foreach($services as $service): ?>
      <tr>
        <td><?=esc($service['package_type'])?></td>
        <td><?=esc($service['session_type'])?></td>
        <td><?=esc($service['photo_quality'])?></td>
        <td><?=esc($service['photo_size'])?></td>
        <td><?=esc($service['no_of_photos'])?></td>
        <td><?=esc($service['delivery_method'])?></td> 
        <td>$<?=esc($service['price'])?></td> 
        <td><?=esc($service['image'])?></td>
        <td><?=esc($service['photographer_name'])?></td>
        <td>
          <a href="edit.php?id=<?=esc_attr($service['services_id']);?>" class="btn btn-default">Edit</a>
          <a href="services.php?target_id=<?=esc_attr($service['services_id']);?>" class="btn btn-default">Delete</a>
        </td>
      </tr>
      <?php endforeach; ?>
  </table>  
<?php endif; ?>
<!--  including footer file -->
<?php require __DIR__.'/inc/footer.inc.php'; ?>