<?php

  require __DIR__.'/../../config.php';
    /**
     * Contact Page 
     * last_update: 2019-08-02
     * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
     * Site name : Khera Digital Studio and Color Lab
     */


  require __DIR__.'/../../autoloaded.php';

  // using services from models
  use App\ServiceModel;
  use App\Validator;
  
  // instantiating the object
  $e = new ServiceModel;
  $v = new Validator();
  
  // if statement to check request method
  if('GET' == $_SERVER['REQUEST_METHOD']){
    if(isset($_GET['id'])){
      $id = $_GET['id'];
      $services = $e->one($id);
    }
  }
  
  // if conditino check for post request method
  if('POST' == $_SERVER['REQUEST_METHOD']){
    
    $id = $_REQUEST['services_id'];
    $services = $e->one($id);
    // checking for required fields
    $v->required('package_type');
    $v->required('session_type');
    $v->required('photo_quality');
    $v->required('photo_size');
    $v->required('no_of_photos');
    $v->required('delivery_method');
    $v->required('price');
    $v->required('photographer_name');
    
    // check for validation of fiels
    $v->stringCheck('package_type');
    $v->sessionQuality('session_type');
    $v->sessionQuality('photo_quality');
    $v->size('photo_size');
    $v->quantity('no_of_photos');
    $v->stringCheck('delivery_method');
    $v->price('price');
    $v->stringCheck('photographer_name');
    
    // lenth validation
    $v->length('package_type');
    $v->length('photographer_name');
    
    // for displaying error
    $errors = $v->errors();
    // check for errro counting
    if(!count($errors)){
      $rows = $e->update($_POST);
      //var_dump($rows);  
      if($rows==0){
      // getting the id 
      //$id = $_REQUEST['id'];    
      // redirection to othe rpage
      header('Location:services.php');
      $_SESSION['message'] = "Data updated sucessfully!!";  
      die;
      }// endif of $rows==0
    }// end of count errors
    $services = $_POST;
  }// end of post if 
 
  require __DIR__.'/inc/head.inc.php';
?>
  <!-- cart information -->
  <?php if(empty($_SESSION['admin']) || $_SESSION['admin'] != true) : ?>
    <header style="min-height: 700px;padding-left: 60px">
      <?php 
      $_SESSION['message'] = "You have to login to see the records!!";
      header('Location:/../login.php');
      //require __DIR__.'/../../inc/message.inc.php'; ?>
      <h1><?=esc($_SESSION['message'])?></h1>
    </header>
  <?php else : ?>
	
  <header>
    <h1>Edit a Table</h1>	
  </header>
  <!-- form to diaply the data that for editing -->
  <div class="form">
    <form action="edit.php" method="post">
      <!-- cref protection -->
      <input type="hidden" name="csrf" value="<?=esc_attr($_SESSION['csrf']);?>" />
      <p>
        <label for="services_id"></label>
        <input type="hidden" name="services_id" id="services_id" value="<?=esc_attr($services['services_id'])?>" />
      </p><br /> 
      <!-- Package type -->
      <p>
        <label for="package_type" class="field">Package</label>
        <input type="text" name="package_type" id="package_type" value="<?=esc_attr($services['package_type'])?>" />
        <?php if(!empty($errors['package_type'])) : ?>
          <span class="error"><?=esc($errors['package_type'])?></span>
        <?php endif; ?>
      </p>
      <!-- session type -->
      <p> 
        <label for="session_type" class="field">Session time</label>
        <input type="text" name="session_type" id="session_type" value="<?=esc_attr($services['session_type'])?>" />
        <?php if(!empty($errors['session_type'])) : ?>
          <span class="error"><?=esc($errors['session_type'])?></span>
        <?php endif; ?>
      </p>
      <!-- photo quality -->
      <p>
        <label for="photo_quality" class="field">Quality</label>
        <input type="text" name="photo_quality" id="photo_quality" value="<?=esc_attr($services['photo_quality'])?>" />
        <?php if(!empty($errors['photo_quality'])) : ?>
          <span class="error"><?=esc($errors['photo_quality'])?></span>
        <?php endif; ?>
      </p>
      <!-- photo size --> 
      <p>
        <label for="photo_size" class="field">Size</label>
        <input type="text" name="photo_size" id="photo_size" value="<?=esc_attr($services['photo_size'])?>" />
        <?php if(!empty($errors['photo_size'])) : ?>
          <span class="error"><?=esc($errors['photo_size'])?></span>
        <?php endif; ?>
      </p>
      <!-- no of photos --> 
      <p>
        <label for="no_of_photos" class="field">Quantity</label>
        <input type="text" name="no_of_photos" id="no_of_photos" value="<?=esc_attr($services['no_of_photos'])?>" />
        <?php if(!empty($errors['no_of_photos'])) : ?>
          <span class="error"><?=esc($errors['no_of_photos'])?></span>
        <?php endif; ?>
      </p>
      <!-- delivery method -->
      <p>
        <label for="delivery_method" class="field">Delivery type</label>
        <input type="text" name="delivery_method" id="delivery_method" value="<?=esc_attr($services['delivery_method'])?>" />
        <?php if(!empty($errors['delivery_method'])) : ?>
          <span class="error"><?=esc($errors['delivery_method'])?></span>
        <?php endif; ?>
      </p>
      <!-- price -->
      <p>
        <label for="price" class="field">Price</label>
        <input type="text" name="price" id="price" value="<?=esc_attr($services['price'])?>" />
        <?php if(!empty($errors['price'])) : ?>
          <span class="error"><?=esc($errors['price'])?></span>
        <?php endif; ?>
      </p>
      <!-- photographer name-->  
      <p>
        <label for="photographer_name" class="field">Photographer</label>
        <input type="text" name="photographer_name" id="photographer_name" value="<?=esc_attr($services['photographer_name'])?>" />
        <?php if(!empty($errors['photographer_name'])) : ?>
          <span class="error"><?=esc($errors['photographer_name'])?></span>
        <?php endif; ?>
      </p>
      <!-- submit button -->
      <button type="submit" value="update">Click to Update</button>  
    </form>
  </div>
<?php endif; ?>
<?php require __DIR__.'/inc/footer.inc.php'; ?>

