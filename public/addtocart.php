<?php 
  require __DIR__.'/../config.php';
  /**
   * Service Page 
   * last_update: 2019-08-02
   * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
   * Site name : Khera Digital Studio and Color Lab
   */
  
  require __DIR__.'/../autoloaded.php';
  $title = "Add to cart";
  // check for login
  if(empty($_SESSION['log_in']) || $_SESSION['log_in'] != true){
    // setting meassage
    $_SESSION['message'] = 'You have to login to book your service';
    // page in session named target 
    $_SESSION['target'] = 'addtocart.php';
    //PRG redirection to login page
    header("Location:login.php");
    die();
  }

  // using models and child of models
  use App\ServiceModel;
  use App\Cart;
  use App\CustomerModel;
  use App\Validator;
  use App\OrderModel;
  // instantiating the object
  $s = new ServiceModel;
  $c = new Cart;
  $cust = new CustomerModel;
  $v = new Validator();
  $o = new OrderModel();
  //check for reuest for id
  if('GET' == $_SERVER['REQUEST_METHOD']){
    // check for id existance
    if(isset($_GET['id'])){   
          $_SESSION['cart_item']  = $_GET['id'];
      }
  } 
  // check for session cart_item exists or not  
  if(!empty($_SESSION['cart_item'])){
    $id=$_SESSION['cart_item'];
    //$id = $_SESSION['cart_item
    // getting all data for the serevice
    $services = $s->one($id);
   // var_dump($custinfo);
    // get the price fro the service
    $price = $services['price'];
    // value changed to interger from string
    $price = intval($price);
    // getting the subtotal
    $subtotal = $c->getSubTotal($price);
    // calulate gst
    $gst = $c->gst($price);
    // calcualring pst
    $pst = $c->pst($price);
    // gettign the main total for the services.
    $total = $c->gettotal($price);
  }

  // get the customer data
  if(isset($_SESSION['log_in'])){
    $id = $_SESSION['customer_id'];
    $custinfo = $cust->one($id);
  }
  // check for post method
  if('POST' == $_SERVER['REQUEST_METHOD']){
    // vaidations for pay now form
      $v->required('name');
      $v->required('cardno');
      $v->required('cvv');
      $v->required('expiry_date');

      //field validation
      $v->stringCheck('name');
      $v->cardCheck('cardno');
      $v->cvvCheck('cvv');
      $v->date('expiry_date');
    // for displaying error
    $errors = $v->errors();

    // database entry start
    if(!count($errors)){
      $name = $custinfo['first_name']." ".$custinfo['last_name'];
      $address = $custinfo['city']." ".$custinfo['province']." ".$custinfo['country']." ".$custinfo['postal_code'];
      $target_id = $o->addOrder($services['services_id'],$custinfo['customer_id'],$name,$address,$custinfo['phone'],$price,$gst,$pst,$total);

      if($target_id){
      //
      unset($_SESSION['cart_item']);
      header('Location:checkout.php?target_id=' . $target_id);
      }else{
        die("Something goes wrong..");
      }
    }
  }


  // adding main head file
  require __DIR__.'/../inc/head.inc.php';


?>
  
  <body>
   
    <!-- header PHP file -->
    <?php
    // adding header file
    require __DIR__.'/../inc/header.inc.php'; 
    ?>
    
    <section>
      <div id="container"><!-- container div started -->
        <div id="inner"><!-- inner div started -->
          <a class="back" href="services.php" title="service page">Back to Services</a>
          <!-- condition to check is exits or not -->
          <?php if(!empty($id)) : ?>
          <h1><span class="underline">Your information:</span></h1>

          <!--customer_data-->
          <div id="info">
            <p><span style="font-size: 20px">Customer name:</span> <span style="color:#ff0;"><?=ucwords(esc($custinfo['first_name']));?> <?=ucwords($custinfo['last_name'])?></span></p>

            <address><span style="font-size: 20px">Address:</span>
              <span style="color:#ff0;"><?=ucwords(esc($custinfo['street']));?><br />
              <?=ucwords(esc($custinfo['city']));?>,
              <?=ucwords(esc($custinfo['province']));?><br />
              <?=ucwords(esc($custinfo['country']));?>,
              <?=ucwords(esc($custinfo['postal_code']));?>.<br /></span>
            </address>
            <p><span style="font-size: 20px">Phone no.</span> <span style="color:#ff0;"><?=esc($custinfo['phone'])?></span></p>
          </div>
          <hr />
          <!-- table to display data for the booked service -->
          <table>
            <caption><?=esc($services['package_type'])?></caption>
            <tr style="background: #fff;color:#300;">
              <td><strong>Quality</strong></td>
              <td><?=esc($services['photo_quality'])?></td>
            </tr>
            <tr style="background: #fff;color:#300;">
              <td><strong>Session Time</strong></td>
              <td><?=esc($services['session_type'])?></td>
            </tr>
            <tr style="background: #fff;color:#300;">
              <td><strong>Delivery Method</strong></td>
              <td><?=esc($services['delivery_method'])?></td>
            </tr>
            <!-- price infromation -->
            <tr>
              <td><strong>Price</strong></td>
              <td>$<?=$subtotal;?></td>
            </tr>
            <tr>
              <td><strong>GST</strong></td>
              <td>$<?=$gst;?></td>
            </tr>
            <tr>
              <td><strong>PST</strong></td>
              <td>$<?=$pst;?></td>
            </tr>
            <!-- pritn total price -->
            <tr style="background: #000;color:#fff;">
              <td><strong>Total Price</strong></td>
              <td>$<?=$total;?></td>
            </tr>
          </table>
          <!-- Form to get payment in formation -->
          <form id="pay_form"
                name="pay_form"
                method="post"
                action="<?=esc_attr($_SERVER['PHP_SELF'])?>"
                > <!-- starting of request form -->
            <fieldset>
              <legend>CARD Information</legend>
              <!-- mandatory field info -->
              <p>All the (<span style="color:#f00;">*</span>) fields are mandatory</p>
              <!-- cref protection -->
              <input type="hidden" name="csrf" value="<?=esc_attr($_SESSION['csrf'])?>" />
               <!-- cardholder name -->             
              <p>
                <label for="name" class="field">Cardholder name: </label>
                  <input type="text" 
                         name="name" 
                         id="name" 
                         placeholder="Enter your name"
                          /><!-- email input field -->
                  <?php if(!empty($errors['name'])) : ?>
                    <span class="error"><?=esc($errors['name'])?></span>
                  <?php endif; ?>
              </p>
              <!-- card number -->
              <p>
                <label for="cardno" class="field">Card number: </label>
                  <input type="text" 
                         name="cardno" 
                         id="cardno" 
                         placeholder=" Enter 16 digit card no."
                          /><!-- email input field -->
                  <?php if(!empty($errors['cardno'])) : ?>
                    <span class="error"><?=esc($errors['cardno'])?></span>
                  <?php endif; ?>
              </p>
              <!-- expiry_date -->
              <p>
                <label for="expiry_date" class="field">Expiry Date: </label>
                  <input type="text" 
                         name="expiry_date" 
                         id="expiry_date"
                         placeholder="MM/YYYY"
                          /><!-- email input field -->
                  <?php if(!empty($errors['expiry_date'])) : ?>
                    <span class="error"><?=esc($errors['expiry_date'])?></span>
                  <?php endif; ?>
              </p>
              <!-- cvv number -->
              <p>
                <label for="cvv" class="field">CVV: </label>
                  <input type="password" 
                         name="cvv" 
                         id="cvv" 
                         placeholder=" Enter 3 digit CVV"
                         /><!-- password input field -->
                  <?php if(!empty($errors['cvv'])) : ?>
                    <span class="error"><?=esc($errors['cvv'])?></span>
                  <?php endif; ?>
              </p>

              <!-- action performed by submit and resrt button -->
              <button >PAY NOW</button> 
            </fieldset>
          </form>
          <!--check for errro meassage -->
        <?php else : ?>
            <?php 
            $_SESSION['message'] = "You have to Book service to view the cart information!!";
            require __DIR__.'/../inc/message.inc.php'; ?>
          <?php endif; ?>
        </div><!-- inner div ending -->
      </div><!-- container div ending -->
     
    </section>
    
    <!-- Footer PHP File -->
    <?php 
    // adding footer file 
    require __DIR__.'/../inc/footer.inc.php'; 
    ?>   
  </body>
</html>