   <?php 
  require __DIR__.'/../config.php';
  /**
   * Contact Page 
   * last_update: 2019-08-02
   * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
   * Site name : Khera Digital Studio and Color Lab
   */
  
  
  require __DIR__.'/../autoloaded.php';

  // list class dependecies in USE statments
  use App\Validator;

  // creating object
  $validator_obj = new Validator();

  $title = "Contact"; 

  // check for post method by server request
  if('POST' == $_SERVER['REQUEST_METHOD']){

    //check for required field
    $validator_obj->required('first_name');
    $validator_obj->required('email_address');
    $validator_obj->required('comments');
    //field validation 
    $validator_obj->stringCheck('first_name');
    $validator_obj->emailCheck('email_address');

    $errors = $validator_obj->errors();
    // database entry start
    if(!count($errors)){
      $query = 'INSERT INTO 
                comments
                (first_name, email_address, comments)
                Values
                (:first_name, :email_address, :comments)';
      //query preparation 
      $stmt = $dbh->prepare($query);
      
      // parameters array s
      $params = array(
          ':first_name' => $_POST['first_name'],
          ':email_address' => $_POST['email_address'],
          ':comments' => $_POST['comments']
      );
      
      //query execution
      $stmt->execute($params);
      header('Location:contact.php');      
      $_SESSION['message'] = "Thank you to contact us";
    }// end of empty errors
    else{
      $_SESSION['message'] = "Something goes wrong!";
    }
  }// end of post method

  // Adding man head file
  require __DIR__.'/../inc/head.inc.php';
?>
  
  <body>
   
    <!-- header PHP file -->
    <?php
    // adding header file
    require __DIR__.'/../inc/header.inc.php'; 
    ?>
    
    <div id="container"><!-- container div started -->
      <div id="inner"><!-- inner div started -->
        <div id="contact_info"><!-- content_info div starting -->
          <div>
            <h1><span class="underline">Contact us:</span></h1>
            <?php require __DIR__.'/../inc/message.inc.php'; ?>
            <div class="two_col"><!-- two_col starting -->
              <h2>Address</h2>
              <address>
                Amritsar Road,<br />
                Near Khanna Hospital,<br />
                Moga,Punjab,India,<br />
                142001. 
              </address>
              <h2>Timings:</h2>
              <p>10.00 AM - 8.00 PM</p>
              <h2>Call on:</h2>
              <p>97812-40006, 97816-04346</p>
              <h2>E-mail:</h2>
              <p>kheracolorlabmoga@gmail.com</p>
            </div><!-- two_col ending -->
            <img src="Images/card.jpg" alt="card" />
          </div>
        </div><!-- content_info div ending -->
        <hr />
        <br />
        <div style="margin-top:30px;margin-bottom: 50px;"><!-- table div starting -->
          <table><!-- table starting -->
            <caption>Package information</caption>
            <tr >
              <th colspan="4" style="background: #fcf;">PACKAGE PRICES</th>
            </tr>
            <tr>
              <th>Package 1</th>
              <th>Package 2</th>
              <th>Package 3</th>
              <th>Package 4</th>
            </tr>
            <tr>
              <td>$150</td>
              <td>$250</td>
              <td>$350</td>
              <td>$450</td>
            </tr>
            <tr>
              <td>1 Person</td>
              <td>1-3 Person(s)</td>
              <td>1-2 Person(s)</td>
              <td>1-6 Person(s)</td>
            </tr>
            <tr>
              <td>15 Mintute Session</td>
              <td>20 Mintute Session</td>
              <td>30 Mintute Session</td>
              <td>45 Mintute Session</td>
            </tr>
            <tr>
              <td>10 Digital Images</td>
              <td>20 Digital Images</td>
              <td>30 Digital Images</td>
              <td>40 Digital Images</td>
            </tr>
            <tr>
              <td>Photo release</td>
              <td>Photo release + Facbook Cover</td>
              <td>Photo release</td>
              <td>Photo release + Facebook Cover</td>
            </tr>
          </table><!-- table ending -->
        </div><!-- table div ending -->
        <hr />
        <br />
        <div><!-- div for form started -->
          <h1 style="padding-left: 30px;">Contact Form:</h1>
          <p style="padding-left: 30px;">Please fill the form to chat with us:</p>
          <form id="first_form"
                name="first_form"
                method="post"
                action="<?=esc_attr($_SERVER['PHP_SELF'])?>"
                autocomplete="on" 
                > <!-- starting of request form -->
            <fieldset>
              <legend>Contact Information</legend>
              <p>All the (<span style="color:#f00;">*</span>) fields are mandatory</p>
              <!-- cref protection -->
              <input type="hidden" name="csrf" value="<?=esc_attr($_SESSION['csrf'])?>" />
              <!-- naem field -->
              <p>
                <label for="first_name" class="field">First Name:</label> 
                  <input type="text" 
                         name="first_name" 
                         id="first_name" 
                         maxlength="40"  
                         placeholder="Type Your First name"
                         value="<?=clean('first_name');?>"
                         /><!-- First name field -->
                  <?php if(!empty($errors['first_name'])) : ?>
                    <span class="error"><?=esc($errors['first_name'])?></span>
                  <?php endif; ?>
              </p>
              <!-- email_address field -->
              <p>
                <label for="email_address" class="field">Email: </label>
                  <input type="text" 
                         name="email_address" 
                         id="email_address" 
                         placeholder="e.g: soniaverma@gmail.com" 
                         value="<?=clean('email_address');?>"
                          /><!-- email input field -->
                  <?php if(!empty($errors['email_address'])) : ?>
                    <span class="error"><?=esc($errors['email_address'])?></span>
                  <?php endif; ?>
              </p>

              <!-- comments field -->
              <p>
                <label for="comments" class="field"> Comments: </label> 
                  <input type="text" 
                         name="comments" 
                         id="comments" 
                         maxlength="40"
                         placeholder="Type Your Last name" 
                         value="<?=clean('comments');?>"
                         /><!-- Last name field -->
                  <?php if(!empty($errors['comments'])) : ?>
                    <span class="error"><?=esc($errors['comments'])?></span>
                  <?php endif; ?>
              </p>

              <!-- action performed by submit and resrt button -->
              <button>Submit</button> 
                                      
            </fieldset>

          </form><!-- end of contact form -->
        </div><!-- div for form ending -->
      </div><!-- inner div ending -->
    </div><!-- container div ending -->
    
    <!-- Footer PHP File -->
    <?php
    // adding footer file 
    require __DIR__.'/../inc/footer.inc.php'; 
    ?>  
  </body>
</html>