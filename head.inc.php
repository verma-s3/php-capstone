<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
	  <!-- Bootstrap template is downliaded from 
	       https://tympanus.net/codrops/2013/07/30/google-nexus-website-menu/   
	       
	      -- Google Nexus Website Menu -- 
	 -->
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>Admin Panel</title>
		<meta name="description" content="A sidebar menu as seen on the Google Nexus 7 website" />
		<meta name="keywords" content="google nexus 7 menu, css transitions, sidebar, side menu, slide out menu" />
		<meta name="author" content="Codrops" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" type="text/css" href="/public/styles/normalize.css" />
		<link rel="stylesheet" type="text/css" href="/public/styles/demo.css" />
		<link rel="stylesheet" type="text/css" href="/public/styles/component.css" />
		<script src="/public/js/modernizr.custom.js"></script>
	  <?php require __DIR__.'/../css/edit.css'; ?>
	  <style>
      .message{
        color: #fff;
        background: #5f6f81;
        border: 3px solid #fff;
        margin: 40px;
        padding: 20px;
        text-align: center;
      }
      .search-btn{
        color: #fff;
        background: #5f6f81;
        border: 2px solid #5f6f81;
        padding: 9px;
        margin: 0px;
      }
      .search-btn:hover{
        color: #5f6f81;
        background: #fff;
      }
    </style>
	</head>
	<body>
		<div class="container">
			<ul id="gn-menu" class="gn-menu-main">
				<li class="gn-trigger">
					<a class="gn-icon gn-icon-menu"><span>Menu</span></a>
					<nav class="gn-menu-wrapper">
						<div class="gn-scroller">
							<ul class="gn-menu">
								<li>
									<ul class="gn-submenu">
										<li><a class="gn-icon gn-icon-article" href="#">Services</a></li>
										<li><a class="gn-icon gn-icon-article" href="#">Orders</a></li>
										<li><a class="gn-icon gn-icon-article" href="#">Customers</a></li>
										<li><a class="gn-icon gn-icon-article" href="#">Users</a></li>
									</ul>
								</li>
						  </ul>
						</div><!-- /gn-scroller -->
					</nav>
				</li>
				<li><a href="index.php"><big class="glyphicon glyphicon-user"> ADMIN PANEL</big> ---- Khera Color Lab and Digital Studio</a></li>
<!--				<li><a class="codrops-icon codrops-icon-prev" href="http://tympanus.net/Development/HeaderEffects/"><span>Back</span></a></li>-->
				<li><a class="codrops-icon codrops-icon-drop" href="index.php"><span>Back to HOME Page</span></a></li>
			</ul>